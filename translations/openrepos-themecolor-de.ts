<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de">
<context>
    <name>ColorDirectInput</name>
    <message>
        <source>Set!!</source>
        <translation type="obsolete">Gewählt!!</translation>
    </message>
</context>
<context>
    <name>ColorField</name>
    <message>
        <source>e.g.</source>
        <translation type="vanished">z.B.</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorField.qml" line="10"/>
        <source>specify RGB or aRGB value, e.g.</source>
        <translation>gib einen RGB oder aRGB- Wert ein, z.B.</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorField.qml" line="10"/>
        <source>(the # is optional)</source>
        <translation>(das # ist optional)</translation>
    </message>
</context>
<context>
    <name>ColorRandomizer</name>
    <message>
        <location filename="../qml/components/ColorRandomizer.qml" line="20"/>
        <source>Random</source>
        <translation>Zufällige</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorRandomizer.qml" line="20"/>
        <location filename="../qml/components/ColorRandomizer.qml" line="35"/>
        <location filename="../qml/components/ColorRandomizer.qml" line="56"/>
        <source>Colors</source>
        <translation>Farben</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorRandomizer.qml" line="30"/>
        <location filename="../qml/components/ColorRandomizer.qml" line="51"/>
        <location filename="../qml/components/ColorRandomizer.qml" line="72"/>
        <source>Generated</source>
        <translation>Erzeugte</translation>
    </message>
    <message>
        <source>Generating...</source>
        <translation type="vanished">Generiere...</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorRandomizer.qml" line="35"/>
        <source>Bright</source>
        <translation>helle</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorRandomizer.qml" line="35"/>
        <source>Dark</source>
        <translation>dunkle</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorRandomizer.qml" line="56"/>
        <source>Gray</source>
        <translation>graue</translation>
    </message>
</context>
<context>
    <name>ColorSelectors</name>
    <message>
        <source>Primary</source>
        <translation type="vanished">Primäre</translation>
    </message>
    <message>
        <source>Color</source>
        <translation type="vanished">Farbe</translation>
    </message>
    <message>
        <source>Secondary</source>
        <translation type="vanished">Sekundäre</translation>
    </message>
    <message>
        <source>Highlight</source>
        <translation type="vanished">Hervorhebungs-</translation>
    </message>
    <message>
        <source>Primary Color</source>
        <translation type="vanished">Primärfarbe</translation>
    </message>
    <message>
        <source>Secondary Color</source>
        <translation type="vanished">Sekundärfarbe</translation>
    </message>
    <message>
        <source>Highlight Color</source>
        <translation type="vanished">Farbe für Hervorhebungen</translation>
    </message>
    <message>
        <source>Secondary Highlight Color</source>
        <translation type="vanished">Sekondäre Farbe für Hervorhebungen</translation>
    </message>
</context>
<context>
    <name>ColorSlider</name>
    <message>
        <source>Set!!</source>
        <translation type="vanished">Gewählt!!</translation>
    </message>
    <message>
        <source>Adjust sliders, tap to set</source>
        <translation type="vanished">Tippen zum Anwenden</translation>
    </message>
    <message>
        <source>Locked!</source>
        <translation type="vanished">Gewählt!!</translation>
    </message>
    <message>
        <source>Adjust sliders, tap to lock</source>
        <translation type="vanished">Regler zum Ändern, Tippen zum Wählen</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSlider.qml" line="42"/>
        <source>Adjust sliders, tap to reset</source>
        <translation>Mit den Schiebern ändern. Tippen zum Zurücksetzen</translation>
    </message>
</context>
<context>
    <name>ColorSliders</name>
    <message>
        <source>Primary</source>
        <translation type="vanished">Primäre</translation>
    </message>
    <message>
        <source>Color</source>
        <translation type="vanished">Farbe</translation>
    </message>
    <message>
        <source>Secondary</source>
        <translation type="vanished">Sekundäre</translation>
    </message>
    <message>
        <source>Highlight</source>
        <translation type="vanished">Hervorhebungs-</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSliders.qml" line="13"/>
        <source>Primary Color</source>
        <translation>Primärfarbe</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSliders.qml" line="18"/>
        <source>Secondary Color</source>
        <translation>Sekundärfarbe</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSliders.qml" line="23"/>
        <source>Highlight Color</source>
        <translation>Farbe für Hervorhebungen</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSliders.qml" line="28"/>
        <source>Secondary Highlight Color</source>
        <translation>Sekundäre Farbe für Hervorhebungen</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSliders.qml" line="33"/>
        <source>Highlight Background Color</source>
        <translation>Hevorgehobene Hintergrundfarbe</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSliders.qml" line="38"/>
        <source>Background Glow Color</source>
        <translation>Hintergrundleuchtfarbe</translation>
    </message>
</context>
<context>
    <name>ColorSwapper</name>
    <message>
        <location filename="../qml/components/ColorSwapper.qml" line="66"/>
        <source>Copy</source>
        <translation>Kopieren</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSwapper.qml" line="66"/>
        <source>Swap</source>
        <translation>Tauschen</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSwapper.qml" line="83"/>
        <location filename="../qml/components/ColorSwapper.qml" line="104"/>
        <source>Primary Color</source>
        <translation>Primärfarbe</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSwapper.qml" line="84"/>
        <location filename="../qml/components/ColorSwapper.qml" line="105"/>
        <source>Secondary Color</source>
        <translation>Sekundärfarbe</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSwapper.qml" line="85"/>
        <location filename="../qml/components/ColorSwapper.qml" line="106"/>
        <source>Highlight Color</source>
        <translation>Farbe für Hervorhebungen</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSwapper.qml" line="86"/>
        <location filename="../qml/components/ColorSwapper.qml" line="107"/>
        <source>Secondary Highlight Color</source>
        <translation>Sekundäre Farbe für Hervorhebungen</translation>
    </message>
    <message>
        <source>Color</source>
        <translation type="obsolete">Farbe</translation>
    </message>
</context>
<context>
    <name>ColorTextInput</name>
    <message>
        <source>Locked!</source>
        <translation type="vanished">Gewählt!!</translation>
    </message>
    <message>
        <source>Input value, tap to lock</source>
        <translation type="vanished">Werte eingeben. Tippen zum Wählen</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorTextInput.qml" line="31"/>
        <source>Color input</source>
        <translation>Farbeingabe</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorTextInput.qml" line="73"/>
        <source>Input value, tap to reset</source>
        <translation>Wert eingeben. Tippen zum Zurücksetzen</translation>
    </message>
</context>
<context>
    <name>ColorTextInputs</name>
    <message>
        <location filename="../qml/components/ColorTextInputs.qml" line="13"/>
        <source>Primary Color</source>
        <translation>Primärfarbe</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorTextInputs.qml" line="18"/>
        <source>Secondary Color</source>
        <translation>Sekundärfarbe</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorTextInputs.qml" line="23"/>
        <source>Highlight Color</source>
        <translation>Hervorhebungen</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorTextInputs.qml" line="28"/>
        <source>Secondary Highlight Color</source>
        <translation>Sekundäre Hervorhebungen</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorTextInputs.qml" line="33"/>
        <source>Highlight Background Color</source>
        <translation>Hevorgehobene Hintergrundfarbe</translation>
    </message>
</context>
<context>
    <name>CoverPage</name>
    <message>
        <location filename="../qml/cover/CoverPage.qml" line="22"/>
        <source>ThemeColor</source>
        <translation>ThemeColor</translation>
    </message>
</context>
<context>
    <name>FirstPage</name>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="37"/>
        <source>Adjust Theme Colors</source>
        <translation>Farbanpassung</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="56"/>
        <source>Laboratory</source>
        <translation>Labor</translation>
    </message>
    <message>
        <source>Current Color Model:</source>
        <translation type="vanished">Farbmodell</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="70"/>
        <source>Tap to switch</source>
        <translation>Tippen zum Umstellen</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="69"/>
        <source>Input Mode:</source>
        <translation>Eingabemodus:</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="39"/>
        <source>Showroom</source>
        <translation>Auslage</translation>
    </message>
    <message>
        <source>Primary Color</source>
        <translation type="vanished">Primärfarbe</translation>
    </message>
    <message>
        <source>Secondary Color</source>
        <translation type="vanished">Sekundärfarbe</translation>
    </message>
    <message>
        <source>Highlight Color</source>
        <translation type="vanished">Hervorhebungen</translation>
    </message>
    <message>
        <source>Secondary Highlight Color</source>
        <translation type="vanished">Sekundäre Hervorhebungen</translation>
    </message>
    <message>
        <source>Apply colors to system</source>
        <translation type="vanished">Farben aufs System anwenden</translation>
    </message>
    <message>
        <source>Reload colors from system</source>
        <translation type="vanished">Farben vom System holen</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="75"/>
        <source>Swapper/Copier</source>
        <translation>Tauschen/Kopieren</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="128"/>
        <source>Open Ambience Settings</source>
        <translation>Ambienteeinstellungen öffnen</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="131"/>
        <source>Compute all Colors from Highlight</source>
        <translation>Alle Farben neu berechnen</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="132"/>
        <location filename="../qml/pages/FirstPage.qml" line="135"/>
        <source>Applying</source>
        <translation>Anwenden</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="134"/>
        <source>Apply Colors to System</source>
        <translation>Farben aufs System anwenden</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="156"/>
        <location filename="../qml/pages/FirstPage.qml" line="159"/>
        <source>Resetting</source>
        <translation>Zurücksetzen</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="162"/>
        <source>Restarting</source>
        <translation>Starte neu</translation>
    </message>
    <message>
        <source>Reload Colors from System</source>
        <translation type="vanished">Farben vom System holen</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="146"/>
        <source>Experimental or dangerous actions</source>
        <translation>Experimentelle oder gefährliche Aktionen</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="147"/>
        <source>Export to Ambience file</source>
        <translation>In Ambientedatei exportieren</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="147"/>
        <location filename="../qml/pages/FirstPage.qml" line="151"/>
        <source>(not implemented)</source>
        <translation>(nicht fertig)</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="152"/>
        <source>Saving</source>
        <translation>Speichere</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="155"/>
        <source>Reset all values and restart</source>
        <translation>Alle Werte zurücksetzen und neu starten</translation>
    </message>
    <message>
        <source>Apply Colors to current Theme</source>
        <translation type="vanished">Farben auf aktuelles Schema anwenden</translation>
    </message>
    <message>
        <source>Reload Colors</source>
        <translation type="vanished">Farben neu laden</translation>
    </message>
    <message>
        <source>Apply to current Theme</source>
        <translation type="vanished">Auf aktuelles Schema anwenden</translation>
    </message>
    <message>
        <source>Applying...</source>
        <translation type="vanished">Anwenden...</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="137"/>
        <source>Reload Colors from current Theme</source>
        <translation>Farben neu laden (vom Theme)</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="140"/>
        <source>Reload Colors from System Config</source>
        <translation>Farben neu laden (aus der Konfig)</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="151"/>
        <source>Save Theme to current Ambience</source>
        <translation>Schema auf aktuelles Ambiente speichern</translation>
    </message>
    <message>
        <source>Randomize</source>
        <translation type="vanished">Würfle</translation>
    </message>
    <message>
        <source>all</source>
        <translation type="vanished">alle</translation>
    </message>
    <message>
        <source>Generating...</source>
        <translation type="vanished">Generiere...</translation>
    </message>
    <message>
        <source>Generate</source>
        <translation type="vanished">Generiere</translation>
    </message>
    <message>
        <source>bright</source>
        <translation type="vanished">helle</translation>
    </message>
    <message>
        <source>gray</source>
        <translation type="vanished">graue</translation>
    </message>
    <message>
        <source>Primary</source>
        <translation type="vanished">Primäre</translation>
    </message>
    <message>
        <source>Color</source>
        <translation type="vanished">Farbe</translation>
    </message>
    <message>
        <source>Secondary</source>
        <translation type="vanished">Sekundäre</translation>
    </message>
    <message>
        <source>Highlight</source>
        <translation type="vanished">Hervorhebungs-</translation>
    </message>
    <message>
        <source>Saving...</source>
        <translation type="vanished">Speichern...</translation>
    </message>
    <message>
        <source>Remove custom values from configuration</source>
        <translation type="vanished">Modifizierte Werte aus der Konfiguration entfernen</translation>
    </message>
    <message>
        <source>Resetting...</source>
        <translation type="vanished">Zurücksetzen...</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="158"/>
        <source>Reset nonstandard values</source>
        <translation>Nicht-Standardwerte zurücksetzen</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="161"/>
        <source>Restart Lipstick</source>
        <translation>Lipstick neu starten</translation>
    </message>
    <message>
        <source>Restarting...</source>
        <translation type="vanished">Neustart...</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="125"/>
        <source>Help</source>
        <translation>Hilfe</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="73"/>
        <source>Sliders</source>
        <translation>Schieber</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="74"/>
        <source>Text</source>
        <translation>Text</translation>
    </message>
    <message>
        <source>Swapper</source>
        <translation type="vanished">Swapper</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="76"/>
        <source>Randomizer</source>
        <translation>Zufallsgenerator</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="77"/>
        <source>Jolla Original</source>
        <translation>Original Jolla</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="110"/>
        <source></source>
        <translation></translation>
    </message>
    <message>
        <source>Open Palette Cupboard</source>
        <translation type="vanished">Palettenschrank öffnen</translation>
    </message>
    <message>
        <source>Reset Colors</source>
        <translation type="vanished">Farben zurücksetzen</translation>
    </message>
</context>
<context>
    <name>HelpPage</name>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="19"/>
        <source>How to Use</source>
        <translation>Anwendung</translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="26"/>
        <source>General</source>
        <translation>Allgemeines</translation>
    </message>
    <message>
        <source>This application allows you to modify the current color scheme of Lipstick. It will not (yet) however, change or create new Ambiences, 
nor will your changes survive an Ambience change, Lipstick restart, or device reboot.&lt;br /&gt;
Currently, only some colors can be edited.
There are other colors in use by the system, which are autocomputed from the basic four, and can not be modified.&lt;br/&gt;&lt;br/&gt;
We are working on overcoming some of these limitations.</source>
        <translation type="vanished">Die App erlaubt das Verändern des momentanen Farbschemas von Lipstick. Sie kann aber (noch) keine Ambiente ändern oder neue erstellen.
Die Änderungen überleben auch keinen Ambientewechel, Lipstick- oder Geräteneustart.&lt;br /&gt;
Zur Zeit können nur einige Farben geändert werden.
Das System nutzt auch einige weitere die von den Basisfarben weg berechnet werden, und nicht modifiziert werden können.&lt;br/&gt;&lt;br/&gt;
An der Lösung dieser und anderer Unzulänglichkeiten wird gearbeitet.</translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="35"/>
        <source>The Showroom</source>
        <translation>Die Auslage</translation>
    </message>
    <message>
        <source>The Top area on the first page (&quot;Showroom&quot;) is non-interactive          and just shows the colors that are selected currently.&lt;br /&gt;          Here you can preview your creation.          </source>
        <translation type="vanished">Der obere Bereich auf der ersten Seite (&quot;Showroom&quot;) ist nicht interaktiv und zeigt die momentan eingestellten Farben&lt;br /&gt;Hier kannst du deine Kreation als Vorschau betrachten.          </translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="41"/>
        <source>The Laboratory</source>
        <translation>Das Labor</translation>
    </message>
    <message>
        <source>In Slider input mode, use the Sliders in the lower section to adjust the colors.  In Text input mode, you can enter color values directly. Randomizer
does what it sais, and Jolla Original you already know.  Swapper lets you change color definitions.&lt;br /&gt; Check what your theme will look like in the
Showroom display.&lt;br /&gt;
&lt;br /&gt;
When you&apos;re done, use the PullDown Menu to apply the colors to the curent session.</source>
        <translation type="vanished">Im Eingabemodus „Schieber“, benutze die Schieberegler zum Ändern der Farben. Im Textmodus kannst du Werte direkt eingeben. Der Zufallszahlengenerator tut genau das, und „Original Jolla“ kennst du schon. Swapper kann Farbcodes tauschen.&lt;br /&gt;Prüfe das Aussehen deines Themas im Showroom.&lt;br /&gt;
&lt;br /&gt;
Wenn alles erledigt ist, verwende das PullDown Menü um die Farben auf die momentane Session zu übertragen.</translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="50"/>
        <source>The Cupboard</source>
        <translation>Der Palettenschrank</translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="37"/>
        <source>The top area on the first page (&quot;Showroom&quot;) is non-interactive and just shows the colors that are selected currently.&lt;br /&gt;Here you can preview your creation.</source>
        <translation>Der obere Bereich auf der ersten Seite (&quot;Showroom&quot;) ist nicht interaktiv und zeigt die momentan eingestellten Farben.&lt;br /&gt;Hier kannst du deine Kreation als Vorschau betrachten.</translation>
    </message>
    <message>
        <source>This area allows you to store your created palettes for re-use later.  There is one global Cupboard, and one specific for the current Ambience.&lt;br /&gt;
Note that only system-wide Ambiences have a name, custom ones will show as anonymous (for now)</source>
        <translation type="vanished">In diesem Bereich kannst du deine erstellten Paletten für später aufheben. Es gitbt eine Hauptstellage und eine für das momentane Ambiente.&lt;br /&gt;
Nur systemweite Ambiente haben einen Namen, eigens erstellte werden als „inkognito“ dargestellt (zur Zeit)</translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="57"/>
        <source>Tips and Caveats</source>
        <translation>Tipps und Warnungen</translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="28"/>
        <source>This application allows you to modify the current color scheme of Lipstick. It will not (yet) however, change or create new Ambiences, nor will your changes survive an Ambience change, Lipstick restart, or device reboot.&lt;br /&gt;
Currently, only some colors can be edited. There are other colors in use by the system, which are autocomputed from the basic four, and can not be modified.&lt;br/&gt;
&lt;br/&gt;
We are working on overcoming some of these limitations.</source>
        <translation>Die App erlaubt das Verändern des momentanen Farbschemas von Lipstick. Sie kann aber (noch) keine Ambiente ändern oder neue erstellen. Die Änderungen überleben auch keinen Ambientewechel, Lipstick- oder Geräteneustart.&lt;br /&gt;
Zur Zeit können nur einige Farben geändert werden. Das System nutzt auch einige weitere die von den Basisfarben weg berechnet werden, und nicht modifiziert werden können.&lt;br/&gt;
&lt;br/&gt;
An der Behebung dieser und anderer Unzulänglichkeiten wird gearbeitet.</translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="43"/>
        <source>In Slider input mode, use the Sliders in the lower section to adjust the colors. In Text input mode, you can enter color values directly. Randomizer does what it sais, and Jolla Original you already know. Swapper lets you change color definitions.&lt;br /&gt;
Check what your theme will look like in the Showroom display.&lt;br /&gt;
&lt;br /&gt;
When you&apos;re done, use the PullDown Menu to apply the colors to the curent session.</source>
        <translation>Im Eingabemodus „Schieber“, benutze die Schieberegler zum Ändern der Farben. Im Textmodus kannst du Werte direkt eingeben. Der Zufallszahlengenerator tut genau das, und „Original Jolla“ kennst du schon. Swapper kann Farbcodes tauschen.&lt;br /&gt;
Prüfe das Aussehen deines Themas im Showroom.&lt;br /&gt;
&lt;br /&gt;
Wenn alles erledigt ist, verwende das PullDown Menü um die Farben auf die momentane Session zu übertragen.</translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="52"/>
        <source>This area allows you to store your created palettes for re-use later. There is one global Cupboard, and one specific for the current Ambience.&lt;br /&gt;
Note that only system-wide Ambiences have a name, custom ones will show as anonymous (for now)</source>
        <translation>In diesem Bereich kannst du deine erstellten Paletten für später aufheben. Es gitbt eine Hauptstellage und eine für das momentane Ambiente.&lt;br /&gt;
Nur systemweite Ambiente haben einen Namen, eigens erstellte werden als „inkognito“ dargestellt (zur Zeit)</translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="59"/>
        <source>Jolla Ambiences only define four colors, primary (which is black or white depending on Ambience Scheme), secondary which is a little less opaque primary with some added, plus highlight and secondary highlight which are those selected in the Ambience Settings.&lt;br /&gt;
The App lets you edit other colors than these four, but they will not be affected by Ambience changes or Lipstick restarts as they are stored in the dconf database and stay there. This means once applied through the app, they will always stay the same until you change them again in the App.  You can use the reset option documented below to get rid of them if necessary.
&lt;br /&gt;
The app frequently gets confused about colors when switching editing modes, applying colors to system, or taking palettes from the Cupboard. If that happens, try reloading the colors from the system, that helps in most cases.&lt;br /&gt;
&lt;br /&gt;
It is possible to create color schemes which make parts of the UI unreadable. Check especially non-obvious areas such as the virtual Keyboard.&lt;br /&gt;
It&apos;s a good idea to store a known-good color scheme in the Cupboard so you can go restore easily.&lt;br /&gt;
&lt;br /&gt;
If you have messed up the colors somehow, use the PullUp menu option to reset everything, or from command line issue:&lt;br /&gt;
&lt;pre&gt;dconf dump /desktop/jolla/theme/color/&lt;/pre&gt;
&lt;pre&gt;dconf reset /desktop/jolla/theme/color/highlightBackground&lt;/pre&gt;
and repeat for all the other colors stored there. &lt;br /&gt;
Changing the Ambience from the System Settings may also help.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="75"/>
        <source>About</source>
        <translation>Über</translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="76"/>
        <source>Version: </source>
        <translation>Version: </translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="77"/>
        <source>Copyright: </source>
        <translation>Copyright: </translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="78"/>
        <source>License: </source>
        <translation>Lizenz: </translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="79"/>
        <source>Source Code: </source>
        <translation>Quellcode: </translation>
    </message>
</context>
<context>
    <name>LootBoxItem</name>
    <message>
        <location filename="../qml/components/LootBoxItem.qml" line="21"/>
        <location filename="../qml/components/LootBoxItem.qml" line="26"/>
        <source>ThemeColor</source>
        <translation>ThemeColor</translation>
    </message>
    <message>
        <location filename="../qml/components/LootBoxItem.qml" line="25"/>
        <source>A Lootbox was delivered!</source>
        <translation>Eine Lootbox wurde geliefert!</translation>
    </message>
    <message>
        <location filename="../qml/components/LootBoxItem.qml" line="26"/>
        <source>now has more shelves!</source>
        <translation>besitzt jetzt mehr Regalbretter!</translation>
    </message>
    <message>
        <location filename="../qml/components/LootBoxItem.qml" line="26"/>
        <source>Your persistence has been rewarded.</source>
        <translation>Deine Beharrlichkeit wurde belohnt.</translation>
    </message>
    <message>
        <location filename="../qml/components/LootBoxItem.qml" line="27"/>
        <source>Your persistence has been rewarded!</source>
        <translation>Deine Beharrlichkeit wurde belohnt!</translation>
    </message>
    <message>
        <location filename="../qml/components/LootBoxItem.qml" line="37"/>
        <source>Purchase Options</source>
        <translation>Kaufoptionen</translation>
    </message>
    <message>
        <location filename="../qml/components/LootBoxItem.qml" line="46"/>
        <source>Payment* received!</source>
        <translation>Zahlung* erhalten!</translation>
    </message>
    <message>
        <location filename="../qml/components/LootBoxItem.qml" line="46"/>
        <source>Buy more shelves</source>
        <translation>Jetzt mehr Lagerplatz kaufen</translation>
    </message>
    <message>
        <location filename="../qml/components/LootBoxItem.qml" line="51"/>
        <source>Using Jolla Shop credentials to buy Storage Lootbox</source>
        <translation>Kaufe Lagerplatz-Lootbox via Jolla Store</translation>
    </message>
    <message>
        <location filename="../qml/components/LootBoxItem.qml" line="64"/>
        <source>Thank you for your purchase!&lt;br /&gt;Your extra shelves will be delivered in the next update!</source>
        <translation>Danke für deinen Einkauf!&lt;br /&gt;Deine neuen Regalbretter kommen mit dem nächsten Update mit!</translation>
    </message>
    <message>
        <location filename="../qml/components/LootBoxItem.qml" line="74"/>
        <source>*) No seriously there are no in-app-purchases in this app or Lootboxes in the Jolla Shop. That would be ridiculous.&lt;br /&gt;No funds were transfered.&lt;br /&gt; In fact, nothing really happened just now.&lt;br /&gt;…or did it?</source>
        <translation>*) Jetzt mal im Ernst. Natürlich gibts in dieser App keine in-app Käufe oder auch Loot-Boxen im Jolla Shop. Das wär ja lächerlich.&lt;br /&gt;Es ist nichts bezahlt worden.&lt;br /&gt;Genaugenommen ist gerade gar nichts passiert.&lt;br /&gt;… oder doch?</translation>
    </message>
    <message>
        <source>*) No seriously there are no in-app-purchases in this app or Lootboxes in the Jolla Shop. That would be ridiculous.&lt;br /&gt;No funds were transfered.&lt;br /&gt; In fact, nothing really happened just now.&lt;br /&gt;...or did it?</source>
        <translation type="vanished">*) Jetzt mal im Ernst. Natürlich gibts in dieser App keine in-app Käufe oder auch Loot-Boxen im Jolla Shop. Das wär ja lächerlich.&lt;br /&gt;Es ist nichts bezahlt worden.&lt;br /&gt;Genaugenommen ist gerade gar nichts passiert.&lt;br /&gt;… oder doch?</translation>
    </message>
</context>
<context>
    <name>SaveSlot</name>
    <message>
        <source>Storage Shelf</source>
        <translation type="vanished">Regalfach</translation>
    </message>
    <message>
        <location filename="../qml/components/SaveSlot.qml" line="39"/>
        <source>Ambience</source>
        <translation>Ambiente</translation>
    </message>
    <message>
        <location filename="../qml/components/SaveSlot.qml" line="39"/>
        <source>Shelf</source>
        <translation>Regalfach</translation>
    </message>
    <message>
        <source>Storage</source>
        <translation type="vanished">Ablageplatz</translation>
    </message>
    <message>
        <source> Shelf</source>
        <translation type="vanished">Regalfach</translation>
    </message>
    <message>
        <location filename="../qml/components/SaveSlot.qml" line="63"/>
        <source>Take to Lab</source>
        <translation>Mitnehmen</translation>
    </message>
    <message>
        <location filename="../qml/components/SaveSlot.qml" line="64"/>
        <source>Put on Shelf</source>
        <translation>Ablegen</translation>
    </message>
</context>
<context>
    <name>Saver</name>
    <message>
        <source>Palette Cupboard</source>
        <translation type="vanished">Palettenablage</translation>
    </message>
    <message>
        <source>Current Palette</source>
        <translation type="vanished">Momentane Auswahl</translation>
    </message>
    <message>
        <source>Cupboard</source>
        <translation type="vanished">Stellage</translation>
    </message>
    <message>
        <source>Purchase Options</source>
        <translation type="vanished">Kaufoptionen</translation>
    </message>
    <message>
        <source>Buy more shelves</source>
        <translation type="vanished">Kaufe mehr Lagerplatz</translation>
    </message>
    <message>
        <source>Payment* received!</source>
        <translation type="vanished">Zahlung* erhalten!</translation>
    </message>
    <message>
        <source>Using Jolla Shop credentials to buy Storage Lootbox</source>
        <translation type="vanished">Kaufe Lagerplatz-Lootbox via Jolla Store...</translation>
    </message>
    <message>
        <source>Thank you for your purchase!&lt;br /&gt;Your extra shelves will be delivered in the next update!</source>
        <translation type="vanished">Danke für deinen Einkauf!&lt;br /&gt;Deine neuen Regalbretter kommen mit dem nächsten Update mit!</translation>
    </message>
    <message>
        <source>*) No seriously there are no in-app-purchases in this app or Lootboxes in the Jolla Shop. That would be ridiculous.&lt;br /&gt;No funds were transfered.&lt;br /&gt; In fact, nothing really happened just now.&lt;br /&gt;...or did it?</source>
        <translation type="vanished">*) Jetzt mal im Ernst. Natürlich gibts in dieser App keine in-app Käufe oder auch Loot-Boxen im Jolla Shop. Das wär ja lächerlich.&lt;br /&gt;Es ist nichts bezahlt worden.&lt;br /&gt;Genaugenommen ist gerade gar nichts passiert.</translation>
    </message>
    <message>
        <source>*) No seriously there are no in-app-purchases in this app or Lootboxes in the Jolla Shop. That would be ridiculous.&lt;br /&gt;No funds were transfered.&lt;br /&gt; In fact, nothing really happened just now.</source>
        <translation type="vanished">*) Jetzt mal im Ernst. Natürlich gibts in dieser App keine in-app Käufe oder auch Loot-Boxen im Jolla Shop. Das wär ja lächerlich.&lt;br /&gt;Es ist nichts bezahlt worden.&lt;br /&gt;Genaugenommen ist gerade gar nichts passiert.</translation>
    </message>
    <message>
        <location filename="../qml/pages/Saver.qml" line="71"/>
        <source>Global Cupboard</source>
        <translation>Hauptstellage</translation>
    </message>
    <message>
        <location filename="../qml/pages/Saver.qml" line="104"/>
        <source>Clean out this cupboard</source>
        <translation>Entrümpeln</translation>
    </message>
    <message>
        <location filename="../qml/pages/Saver.qml" line="104"/>
        <source>Spring Clean</source>
        <translation>Frühjahrsputz</translation>
    </message>
    <message>
        <source>Spring Clean...</source>
        <translation type="vanished">Frühjahrsputz...</translation>
    </message>
</context>
<context>
    <name>SaverPlus</name>
    <message>
        <location filename="../qml/pages/SaverPlus.qml" line="11"/>
        <source>anonymous</source>
        <translation>incognito</translation>
    </message>
    <message>
        <location filename="../qml/pages/SaverPlus.qml" line="103"/>
        <source>Ambience Cupboard</source>
        <translation>Ambientenstellage</translation>
    </message>
    <message>
        <location filename="../qml/pages/SaverPlus.qml" line="147"/>
        <source>Clean out this cupboard</source>
        <translation>Entrümpeln</translation>
    </message>
    <message>
        <location filename="../qml/pages/SaverPlus.qml" line="147"/>
        <source>Spring Clean</source>
        <translation>Frühjahrsputz</translation>
    </message>
    <message>
        <source>Spring Clean...</source>
        <translation type="vanished">Frühjahrsputz...</translation>
    </message>
    <message>
        <source>Cupboard</source>
        <translation type="vanished">Stellage</translation>
    </message>
    <message>
        <source>Ambience</source>
        <translation type="vanished">Ambiente</translation>
    </message>
    <message>
        <source>Purchase Options</source>
        <translation type="vanished">Kaufoptionen</translation>
    </message>
    <message>
        <source>Payment* received!</source>
        <translation type="vanished">Zahlung* erhalten!</translation>
    </message>
    <message>
        <source>Buy more shelves</source>
        <translation type="vanished">Kaufe mehr Lagerplatz</translation>
    </message>
    <message>
        <source>Using Jolla Shop credentials to buy Storage Lootbox</source>
        <translation type="vanished">Kaufe Lagerplatz-Lootbox via Jolla Store...</translation>
    </message>
    <message>
        <source>Thank you for your purchase!&lt;br /&gt;Your extra shelves will be delivered in the next update!</source>
        <translation type="vanished">Danke für deinen Einkauf!&lt;br /&gt;Deine neuen Regalbretter kommen mit dem nächsten Update mit!</translation>
    </message>
    <message>
        <source>*) No seriously there are no in-app-purchases in this app or Lootboxes in the Jolla Shop. That would be ridiculous.&lt;br /&gt;No funds were transfered.&lt;br /&gt; In fact, nothing really happened just now.&lt;br /&gt;...or did it?</source>
        <translation type="vanished">*) Jetzt mal im Ernst. Natürlich gibts in dieser App keine in-app Käufe oder auch Lootboxen im Jolla Shop. Das wär ja lächerlich.&lt;br /&gt;Es ist nichts bezahlt worden.&lt;br /&gt;Genaugenommen ist gerade gar nichts passiert.</translation>
    </message>
</context>
<context>
    <name>ShowRoom</name>
    <message>
        <source>Showroom</source>
        <translation type="vanished">Auslage</translation>
    </message>
    <message>
        <location filename="../qml/components/ShowRoom.qml" line="89"/>
        <location filename="../qml/components/ShowRoom.qml" line="90"/>
        <location filename="../qml/components/ShowRoom.qml" line="91"/>
        <location filename="../qml/components/ShowRoom.qml" line="92"/>
        <location filename="../qml/components/ShowRoom.qml" line="93"/>
        <source>A very long line showing Text in </source>
        <translation>Eine relativ lange Zeile mit Text in </translation>
    </message>
    <message>
        <source>Primary</source>
        <translation type="vanished">Primäre</translation>
    </message>
    <message>
        <source>Color</source>
        <translation type="vanished">Farbe</translation>
    </message>
    <message>
        <source>Secondary</source>
        <translation type="vanished">Sekundäre</translation>
    </message>
    <message>
        <source>Highlight</source>
        <translation type="vanished">Hervorhebungs-</translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="vanished">Fehler-</translation>
    </message>
    <message>
        <location filename="../qml/components/ShowRoom.qml" line="89"/>
        <location filename="../qml/components/ShowRoom.qml" line="108"/>
        <location filename="../qml/components/ShowRoom.qml" line="136"/>
        <source>Primary Color</source>
        <translation>Primärfarbe</translation>
    </message>
    <message>
        <location filename="../qml/components/ShowRoom.qml" line="90"/>
        <location filename="../qml/components/ShowRoom.qml" line="114"/>
        <location filename="../qml/components/ShowRoom.qml" line="142"/>
        <source>Secondary Color</source>
        <translation>Sekundärfarbe</translation>
    </message>
    <message>
        <location filename="../qml/components/ShowRoom.qml" line="91"/>
        <source>Highlight Color</source>
        <translation>Hervorhebungsfarbe</translation>
    </message>
    <message>
        <location filename="../qml/components/ShowRoom.qml" line="92"/>
        <source>Secondary Highlight Color</source>
        <translation>Sekundär-Hervorhebungsfarbe</translation>
    </message>
    <message>
        <location filename="../qml/components/ShowRoom.qml" line="93"/>
        <source>Error Color</source>
        <translation>Fehlerfallfarbe</translation>
    </message>
    <message>
        <location filename="../qml/components/ShowRoom.qml" line="108"/>
        <location filename="../qml/components/ShowRoom.qml" line="114"/>
        <location filename="../qml/components/ShowRoom.qml" line="136"/>
        <location filename="../qml/components/ShowRoom.qml" line="142"/>
        <source>Background Color</source>
        <translation>Hintergrundfarbe</translation>
    </message>
    <message>
        <source>four kinds of background overlay opacities and colors</source>
        <translation type="vanished">vier Arten von Hintergrunddurchsichtigkeiten</translation>
    </message>
    <message>
        <source>A very long line showing some</source>
        <translation type="vanished">Eine sehr lange Zeile mit</translation>
    </message>
    <message>
        <location filename="../qml/components/ShowRoom.qml" line="108"/>
        <location filename="../qml/components/ShowRoom.qml" line="114"/>
        <location filename="../qml/components/ShowRoom.qml" line="136"/>
        <location filename="../qml/components/ShowRoom.qml" line="142"/>
        <source>Text</source>
        <translation>Text</translation>
    </message>
    <message>
        <location filename="../qml/components/ShowRoom.qml" line="165"/>
        <source>Progress Bar Demo</source>
        <translation>Fortschrittsbalkendemo</translation>
    </message>
    <message>
        <location filename="../qml/components/ShowRoom.qml" line="165"/>
        <source>Tap to restart Demos</source>
        <translation>Tippen zum Demo-Neustart</translation>
    </message>
    <message>
        <location filename="../qml/components/ShowRoom.qml" line="168"/>
        <source>Remorse Item Demo</source>
        <translation>„Remorse Item“ Demo</translation>
    </message>
    <message>
        <location filename="../qml/components/ShowRoom.qml" line="252"/>
        <location filename="../qml/components/ShowRoom.qml" line="253"/>
        <source>Button</source>
        <translation>Knopf</translation>
    </message>
</context>
</TS>
