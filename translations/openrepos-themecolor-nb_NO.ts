<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="nb_NO">
<context>
    <name>ColorField</name>
    <message>
        <location filename="../qml/components/ColorField.qml" line="10"/>
        <source>specify RGB or aRGB value, e.g.</source>
        <translation>angi en RGB- eller aRGB-verdi, f.eks.</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorField.qml" line="10"/>
        <source>(the # is optional)</source>
        <translation>(# er valgfritt)</translation>
    </message>
</context>
<context>
    <name>ColorRandomizer</name>
    <message>
        <location filename="../qml/components/ColorRandomizer.qml" line="20"/>
        <source>Random</source>
        <translation>Tilfeldig</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorRandomizer.qml" line="20"/>
        <location filename="../qml/components/ColorRandomizer.qml" line="35"/>
        <location filename="../qml/components/ColorRandomizer.qml" line="56"/>
        <source>Colors</source>
        <translation>Farger</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorRandomizer.qml" line="30"/>
        <location filename="../qml/components/ColorRandomizer.qml" line="51"/>
        <location filename="../qml/components/ColorRandomizer.qml" line="72"/>
        <source>Generated</source>
        <translation>Generert</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorRandomizer.qml" line="35"/>
        <source>Bright</source>
        <translation>Lyst</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorRandomizer.qml" line="35"/>
        <source>Dark</source>
        <translation>Mørk</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorRandomizer.qml" line="56"/>
        <source>Gray</source>
        <translation>Grå</translation>
    </message>
</context>
<context>
    <name>ColorSelectors</name>
    <message>
        <source>Color</source>
        <translation type="vanished">Colour</translation>
    </message>
    <message>
        <source>Primary Color</source>
        <translation type="vanished">Primary Colour</translation>
    </message>
    <message>
        <source>Secondary Color</source>
        <translation type="vanished">Secondary Colour</translation>
    </message>
    <message>
        <source>Highlight Color</source>
        <translation type="vanished">Highlight Colour</translation>
    </message>
    <message>
        <source>Secondary Highlight Color</source>
        <translation type="vanished">Secondary Highlight Colour</translation>
    </message>
</context>
<context>
    <name>ColorSlider</name>
    <message>
        <location filename="../qml/components/ColorSlider.qml" line="42"/>
        <source>Adjust sliders, tap to reset</source>
        <translation>Juster glidebrytere, trykk for å tilbakestille</translation>
    </message>
</context>
<context>
    <name>ColorSliders</name>
    <message>
        <source>Color</source>
        <translation type="vanished">Colour</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSliders.qml" line="13"/>
        <source>Primary Color</source>
        <translation>Primærfarge</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSliders.qml" line="18"/>
        <source>Secondary Color</source>
        <translation>Sekundærfarge</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSliders.qml" line="23"/>
        <source>Highlight Color</source>
        <translation>Framhevingsfarge</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSliders.qml" line="28"/>
        <source>Secondary Highlight Color</source>
        <translation>Sekundær framhevingsfarge</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSliders.qml" line="33"/>
        <source>Highlight Background Color</source>
        <translation type="unfinished">Framhevelsesbakgrunnsfarge</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSliders.qml" line="38"/>
        <source>Background Glow Color</source>
        <translation>Bakgrunnsglødefarge</translation>
    </message>
</context>
<context>
    <name>ColorSwapper</name>
    <message>
        <location filename="../qml/components/ColorSwapper.qml" line="66"/>
        <source>Copy</source>
        <translation>Kopier</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSwapper.qml" line="66"/>
        <source>Swap</source>
        <translation>Veksle</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSwapper.qml" line="83"/>
        <location filename="../qml/components/ColorSwapper.qml" line="104"/>
        <source>Primary Color</source>
        <translation>Primærfarge</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSwapper.qml" line="84"/>
        <location filename="../qml/components/ColorSwapper.qml" line="105"/>
        <source>Secondary Color</source>
        <translation>Sekundærfarge</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSwapper.qml" line="85"/>
        <location filename="../qml/components/ColorSwapper.qml" line="106"/>
        <source>Highlight Color</source>
        <translation>Framhevelsesfarge</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSwapper.qml" line="86"/>
        <location filename="../qml/components/ColorSwapper.qml" line="107"/>
        <source>Secondary Highlight Color</source>
        <translation>Sekundær framhevelsesfarge</translation>
    </message>
    <message>
        <source>Color</source>
        <translation type="obsolete">Colour</translation>
    </message>
</context>
<context>
    <name>ColorTextInput</name>
    <message>
        <location filename="../qml/components/ColorTextInput.qml" line="31"/>
        <source>Color input</source>
        <translation type="unfinished">Fargeinndata</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorTextInput.qml" line="73"/>
        <source>Input value, tap to reset</source>
        <translation type="unfinished">Inndataverdi, trykk for å tilbakestille</translation>
    </message>
</context>
<context>
    <name>ColorTextInputs</name>
    <message>
        <location filename="../qml/components/ColorTextInputs.qml" line="13"/>
        <source>Primary Color</source>
        <translation>Primærfarge</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorTextInputs.qml" line="18"/>
        <source>Secondary Color</source>
        <translation>Sekundærfarge</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorTextInputs.qml" line="23"/>
        <source>Highlight Color</source>
        <translation>Framhevelsesfarge</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorTextInputs.qml" line="28"/>
        <source>Secondary Highlight Color</source>
        <translation>Sekundær framhevelsesfarge</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorTextInputs.qml" line="33"/>
        <source>Highlight Background Color</source>
        <translation>Framhevelsesbakgrunnsfarge</translation>
    </message>
</context>
<context>
    <name>CoverPage</name>
    <message>
        <location filename="../qml/cover/CoverPage.qml" line="22"/>
        <source>ThemeColor</source>
        <translation>DraktFarge</translation>
    </message>
</context>
<context>
    <name>FirstPage</name>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="37"/>
        <source>Adjust Theme Colors</source>
        <translation>Juster draktfarger</translation>
    </message>
    <message>
        <source>Current Color Model:</source>
        <translation type="vanished">Current Colour Model:</translation>
    </message>
    <message>
        <source>Primary Color</source>
        <translation type="vanished">Primary Colour</translation>
    </message>
    <message>
        <source>Secondary Color</source>
        <translation type="vanished">Secondary Colour</translation>
    </message>
    <message>
        <source>Highlight Color</source>
        <translation type="vanished">Highlight Colour</translation>
    </message>
    <message>
        <source>Secondary Highlight Color</source>
        <translation type="vanished">Secondary Highlight Colour</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="39"/>
        <source>Showroom</source>
        <translation>Visningsrom</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="56"/>
        <source>Laboratory</source>
        <translation>Laboratorium</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="69"/>
        <source>Input Mode:</source>
        <translation type="unfinished">Inndatamodus:</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="70"/>
        <source>Tap to switch</source>
        <translation>Trykk for å bytte</translation>
    </message>
    <message>
        <source>gray</source>
        <translation type="vanished">grey</translation>
    </message>
    <message>
        <source>Apply colors to system</source>
        <translation type="vanished">Apply colours to system</translation>
    </message>
    <message>
        <source>Reload colors from system</source>
        <translation type="vanished">Reload colors from system</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="75"/>
        <source>Swapper/Copier</source>
        <translation>Veksler/kopierer</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="131"/>
        <source>Compute all Colors from Highlight</source>
        <translation>Regn ut alle farger fra framheving</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="132"/>
        <location filename="../qml/pages/FirstPage.qml" line="135"/>
        <source>Applying</source>
        <translation>Legger til …</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="134"/>
        <source>Apply Colors to System</source>
        <translation>Bruk farger for systemet</translation>
    </message>
    <message>
        <source>Reload Colors from System</source>
        <translation type="vanished">Reload Colours from System</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="137"/>
        <source>Reload Colors from current Theme</source>
        <translation>Gjeninnlast farger fra nåværende drakt</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="140"/>
        <source>Reload Colors from System Config</source>
        <translation>Gjeninnlast farger fra systemoppsett</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="146"/>
        <source>Experimental or dangerous actions</source>
        <translation>Eksperimentelle eller farlige handlinger</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="147"/>
        <source>Export to Ambience file</source>
        <translation type="unfinished">Eksporter til omgivelsesfil</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="147"/>
        <location filename="../qml/pages/FirstPage.qml" line="151"/>
        <source>(not implemented)</source>
        <translation>(ikke implementert)</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="151"/>
        <source>Save Theme to current Ambience</source>
        <translation type="unfinished">Lagre drakt til nåværende omgivelse</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="152"/>
        <source>Saving</source>
        <translation>Lagrer …</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="156"/>
        <location filename="../qml/pages/FirstPage.qml" line="159"/>
        <source>Resetting</source>
        <translation>Tilbakestiller …</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="162"/>
        <source>Restarting</source>
        <translation type="unfinished">Starter på ny …</translation>
    </message>
    <message>
        <source>Saving...</source>
        <translation type="vanished">Saving…</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="155"/>
        <source>Reset all values and restart</source>
        <translation type="unfinished">Tilbakestill alle verdier og start på ny …</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="158"/>
        <source>Reset nonstandard values</source>
        <translation>Tilbakestill alle ikke-standardverdier</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="161"/>
        <source>Restart Lipstick</source>
        <translation>Start Lipstick på ny</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="125"/>
        <source>Help</source>
        <translation>Hjelp</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="73"/>
        <source>Sliders</source>
        <translation>Glidebrytere</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="74"/>
        <source>Text</source>
        <translation>Tekst</translation>
    </message>
    <message>
        <source>Swapper</source>
        <translation type="vanished">Swapper</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="76"/>
        <source>Randomizer</source>
        <translation>Tilfeldighetsgenerator</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="77"/>
        <source>Jolla Original</source>
        <translation>Opprinnelig Jolla</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="128"/>
        <source>Open Ambience Settings</source>
        <translation type="unfinished">Åpne omgivelsesinnstillinger</translation>
    </message>
    <message>
        <source>Apply Colors to current Theme</source>
        <translation type="vanished">Apply colours to current Theme</translation>
    </message>
    <message>
        <source>Reload Colors</source>
        <translation type="vanished">Reload Colours</translation>
    </message>
    <message>
        <source>Color</source>
        <translation type="vanished">Colour</translation>
    </message>
    <message>
        <source>Reset Colors</source>
        <translation type="vanished">Reset Colours</translation>
    </message>
</context>
<context>
    <name>HelpPage</name>
    <message>
        <source>Use the Sliders in the lower section to adjust the colors.&lt;br /&gt;            When satisfied, tap the area above the slider to set the color.            Check what your theme will look like in the Showroom display.&lt;br /&gt;            &lt;br /&gt;            When you&apos;re done, use the PullUp Manu or the PushUp Menu to apply your creation.</source>
        <translation type="vanished">Use the Sliders in the lower section to adjust the colours.&lt;br /&gt;            When satisfied, tap the area above the slider to set the colour.            Check what your theme will look like in the Showroom display.&lt;br /&gt;            &lt;br /&gt;            When you&apos;re done, use the PullUp Manu or the PushUp Menu to apply your creation.</translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="19"/>
        <source>How to Use</source>
        <translation>Anvendelse</translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="26"/>
        <source>General</source>
        <translation>Generelt</translation>
    </message>
    <message>
        <source>This application allows you to modify the current color scheme of Lipstick. It will not (yet) however, change or create new ambiences,           nor will your changes survive an Ambience change, Lipstick restart, or device reboot.&lt;br /&gt;
          Currently, only four colors can be edited: primaryColor, secondaryColor, highlightColor, and secondaryHighlightColor, as these are the ones saved by the system.          There are other colors in use by the system, which are autocomputed from these basic four, and can not be modified at the moment.&lt;br/&gt;&lt;br/&gt;
          We are working on overcoming some of these limitations.</source>
        <translation type="vanished">This application allows you to modify the current colour scheme of Lipstick. It will not (yet) however, change or create new ambiences,           nor will your changes survive an Ambience change, Lipstick restart, or device reboot.&lt;br /&gt;
          Currently, only four colours can be edited: primaryColor, secondaryColor, highlightColor, and secondaryHighlightColor (sic!), as these are the ones saved by the system.          There are other colours in use by the system, which are autocomputed from these basic four, and can not be modified at the moment.&lt;br/&gt;&lt;br/&gt;
          We are working on overcoming some of these limitations.</translation>
    </message>
    <message>
        <source>This application allows you to modify the current color scheme of Lipstick. It will not (yet) however, change or create new ambiences,           nor will your changes survive an Ambience change, Lipstick restart, or device reboot.&lt;br /&gt;
          Currently, only some colors can be edited.          There are other colors in use by the system, which are autocomputed from the basic four, and can not be modified at the moment.&lt;br/&gt;&lt;br/&gt;
          We are working on overcoming some of these limitations.</source>
        <translation type="vanished">This application allows you to modify the current colour scheme of Lipstick. It will not (yet) however, change or create new ambiences, nor will your changes survive an Ambience change, Lipstick restart, or device reboot.&lt;br /&gt;
Currently, only some colours can be edited. There are other colours in use by the system, which are autocomputed from the basic four, and can not be modified at the moment.&lt;br/&gt;&lt;br/&gt;
We are working on overcoming some of these limitations.</translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="35"/>
        <source>The Showroom</source>
        <translation>Visningsrommet</translation>
    </message>
    <message>
        <source>The Top area on the first page (&quot;Showroom&quot;) is non-interactive          and just shows the colors that are selected currently.&lt;br /&gt;          Here you can preview your creation.          </source>
        <translation type="vanished">The Top area on the first page (&quot;Showroom&quot;) is non-interactive          and just shows the colours that are selected currently.&lt;br /&gt;          Here you can preview your creation.          </translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="41"/>
        <source>The Laboratory</source>
        <translation>Laboratoriet</translation>
    </message>
    <message>
        <source>In Slider Input Mode, use the Sliders in the lower section to adjust the colors.&lt;br /&gt;            In Text Input Mode, you can enter color values directly.&lt;br /&gt;
            Check what your theme will look like in the Showroom display.&lt;br /&gt;            &lt;br /&gt;            When you&apos;re done, use the PullUp Menu or the PushUp Menu to apply your creation to the Session.
          </source>
        <translation type="vanished">In Slider Input Mode, use the Sliders in the lower section to adjust the colours.&lt;br /&gt;            In Text Input Mode, you can enter colour values directly.&lt;br /&gt;
            Check what your theme will look like in the Showroom display.&lt;br /&gt;            &lt;br /&gt;            When you&apos;re done, use the PullUp Menu or the PushUp Menu to apply your creation to the Session.
          </translation>
    </message>
    <message>
        <source>In Slider input mode, use the Sliders in the lower section to adjust the colors.            In Text input mode, you can enter color values directly. Randomizer does what it sais, and Jolla Original you already know.&lt;br /&gt;             Check what your theme will look like in the Showroom display.&lt;br /&gt;            &lt;br /&gt;            When you&apos;re done, use the PullDown Menu.
          </source>
        <translation type="vanished">In Slider input mode, use the Sliders in the lower section to adjust the colours. In Text input mode, you can enter colour values directly. Randomizer does what it sais, and Jolla Original you already know.&lt;br /&gt;Check what your theme will look like in the Showroom display.&lt;br /&gt; &lt;br /&gt; When you&apos;re done, use the PullDown Menu.
          </translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="50"/>
        <source>The Cupboard</source>
        <translation type="unfinished">Skapet</translation>
    </message>
    <message>
        <source>This area allows you to store your created palettes for re-use later.             There is one global Cupboard, and one specific for the current ambience.&lt;br /&gt;             Note that only system-wide Ambiences have a name, custon ones will show as anonymous (for now)</source>
        <translation type="vanished">This area allows you to store your created palettes for re-use later. There is one global Cupboard, and one specific for the current Ambience.&lt;br /&gt; Note that only system-wide Ambiences have a name, custom ones will show as anonymous (for now)</translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="57"/>
        <source>Tips and Caveats</source>
        <translation>Tips og triks</translation>
    </message>
    <message>
        <source>It is possible to create color schemes which make parts of the UI unreadable. Check especially non-obvious areas such as the virtual Keyboard.&lt;br /&gt;
          It&apos;s a good idea to store a known-good color sceme in the Cupboard so you can go restore easily.&lt;br /&gt;          &lt;br /&gt;          If you have messed up the colours completely, use the PullUp menu option to reset everything, or from command line issue:&lt;br /&gt;          &lt;pre&gt;dconf dump /desktop/jolla/theme/color/&lt;/pre&gt;          &lt;pre&gt;dconf reset /desktop/jolla/theme/color/highlightBackground&lt;/pre&gt;          and repeat for all the other colors stored there. &lt;br /&gt;          Changing the Ambience from the System Settings may also help.</source>
        <translation type="vanished">It is possible to create colour schemes which make parts of the UI unreadable. Check especially non-obvious areas such as the virtual Keyboard.&lt;br /&gt;
It&apos;s a good idea to store a known-good colour scheme in the Cupboard so you can go restore easily.&lt;br /&gt;&lt;br /&gt;If you have messed up the colours completely, use the PullUp menu option to reset everything, or from command line issue:&lt;br /&gt;&lt;pre&gt;dconf dump /desktop/jolla/theme/color/&lt;/pre&gt;&lt;pre&gt;dconf reset /desktop/jolla/theme/color/highlightBackground&lt;/pre&gt;and repeat for all the other colours stored there. &lt;br /&gt;Changing the Ambience from the System Settings may also help.</translation>
    </message>
    <message>
        <source>This application allows you to modify the current color scheme of Lipstick. It will not (yet) however, change or create new Ambiences, 
nor will your changes survive an Ambience change, Lipstick restart, or device reboot.&lt;br /&gt;
Currently, only some colors can be edited.
There are other colors in use by the system, which are autocomputed from the basic four, and can not be modified.&lt;br/&gt;&lt;br/&gt;
We are working on overcoming some of these limitations.</source>
        <translation type="vanished">This application allows you to modify the current colour scheme of Lipstick. It will not (yet) however, change or create new Ambiences, nor will your changes survive an Ambience change, Lipstick restart, or device reboot.&lt;br /&gt;
Currently, only some colours can be edited.
There are other colours in use by the system, which are autocomputed from the basic four, and can not be modified.&lt;br/&gt;&lt;br/&gt;
We are working on overcoming some of these limitations.</translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="37"/>
        <source>The top area on the first page (&quot;Showroom&quot;) is non-interactive and just shows the colors that are selected currently.&lt;br /&gt;Here you can preview your creation.</source>
        <translation>Toppområdet på første side («Visningsrommet») er ikke interaktivt, og viser kun fargene som er valgt.&lt;br /&gt;Her kan du bivåne din kreasjon.</translation>
    </message>
    <message>
        <source>In Slider input mode, use the Sliders in the lower section to adjust the colors.  In Text input mode, you can enter color values directly. Randomizer
does what it sais, and Jolla Original you already know.  Swapper lets you change color definitions.&lt;br /&gt; Check what your theme will look like in the
Showroom display.&lt;br /&gt;
&lt;br /&gt;
When you&apos;re done, use the PullDown Menu to apply the colors to the curent session.</source>
        <translation type="vanished">In Slider input mode, use the Sliders in the lower section to adjust the colours. In Text input mode, you can enter colour values directly. Randomizer
does what it sais, and Jolla Original you already know. Swapper lets you change colour definitions.&lt;br /&gt; Check what your theme will look like in the
Showroom display.&lt;br /&gt;
&lt;br /&gt;
When you&apos;re done, use the PullDown Menu to apply the colours to the current session.</translation>
    </message>
    <message>
        <source>This area allows you to store your created palettes for re-use later.  There is one global Cupboard, and one specific for the current Ambience.&lt;br /&gt;
Note that only system-wide Ambiences have a name, custom ones will show as anonymous (for now)</source>
        <translation type="vanished">This area allows you to store your created palettes for re-use later.  There is one global Cupboard, and one specific for the current Ambience.&lt;br /&gt;
Note that only system-wide Ambiences have a name, custom ones will show as anonymous (for now)</translation>
    </message>
    <message>
        <source>Jolla Ambiences only define four colors, primary (which is black or white depending on Ambience Scheme), secondary which is a little less opaque primary with some added, 
plus highlight and secondary highlight which are those selected in the Ambience Settings.&lt;br /&gt;
The App lets you edit other colors than these four, but they will not be affected by Ambience changes or Lipstick restarts as they are stored in the dconf database and stay there. 
This means once applied through the app, they will always stay the same until you change them again in the App. 
You can use the reset option documented below to get rid of them if necessary.
&lt;br /&gt;
The app frequently gets confused about colors when switching editing modes, applying colors to system, or taking palettes from the Cupboard.
If that happens, try reloading the colors from the system, that helps in most cases.&lt;br /&gt;
&lt;br /&gt;
It is possible to create color schemes which make parts of the UI unreadable. Check especially non-obvious areas such as the virtual Keyboard.&lt;br /&gt;
It&apos;s a good idea to store a known-good color scheme in the Cupboard so you can go restore easily.&lt;br /&gt;
&lt;br /&gt;
If you have messed up the colors somehow, use the PullUp menu option to reset everything, or from command line issue:&lt;br /&gt;
&lt;pre&gt;dconf dump /desktop/jolla/theme/color/&lt;/pre&gt;
&lt;pre&gt;dconf reset /desktop/jolla/theme/color/highlightBackground&lt;/pre&gt;
and repeat for all the other colors stored there. &lt;br /&gt;
Changing the Ambience from the System Settings may also help.</source>
        <translation type="vanished">Jolla Ambiences only define four colours, primary (which is black or white depending on Ambience Scheme), secondary which is a little less opaque primary with some added,
plus highlight and secondary highlight which are those selected in the Ambience Settings.&lt;br /&gt;
The App lets you edit other colours than these four, but they will not be affected by Ambience changes or Lipstick restarts as they are stored in the dconf database and stay there.
This means once applied through the app, they will always stay the same until you change them again in the App.
You can use the reset option documented below to get rid of them if necessary.
&lt;br /&gt;
The app frequently gets confused about colours when switching editing modes, applying colours to system, or taking palettes from the Cupboard.
If that happens, try reloading the colours from the system, that helps in most cases.&lt;br /&gt;
&lt;br /&gt;
It is possible to create colour schemes which make parts of the UI unreadable. Check especially non-obvious areas such as the virtual Keyboard.&lt;br /&gt;
It&apos;s a good idea to store a known-good colour scheme in the Cupboard so you can go restore easily.&lt;br /&gt;
&lt;br /&gt;
If you have messed up the colours somehow, use the PullUp menu option to reset everything, or from command line issue:&lt;br /&gt;
&lt;pre&gt;dconf dump /desktop/jolla/theme/color/&lt;/pre&gt;
&lt;pre&gt;dconf reset /desktop/jolla/theme/color/highlightBackground&lt;/pre&gt;
and repeat for all the other colours stored there. &lt;br /&gt;
Changing the Ambience from the System Settings may also help.</translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="28"/>
        <source>This application allows you to modify the current color scheme of Lipstick. It will not (yet) however, change or create new Ambiences, nor will your changes survive an Ambience change, Lipstick restart, or device reboot.&lt;br /&gt;
Currently, only some colors can be edited. There are other colors in use by the system, which are autocomputed from the basic four, and can not be modified.&lt;br/&gt;
&lt;br/&gt;
We are working on overcoming some of these limitations.</source>
        <translation>Programmet lar deg endre fargepaletten på Lipstick. Det vil ikke (enda) opprette eller endre nye omgivelser, ei heller overlever det endring av omgivelse, omstart av Lipstick, eller omstart av enheten.&lt;br /&gt;
Kun noen farger kan redigeres. Det er andre farger i bruk av systemet som automatisk utregnes fra de grunnleggende fire, og de kan ikke endres.&lt;br/&gt;
&lt;br/&gt;
Det jobbes med å overkomme disse hindrene.</translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="43"/>
        <source>In Slider input mode, use the Sliders in the lower section to adjust the colors. In Text input mode, you can enter color values directly. Randomizer does what it sais, and Jolla Original you already know. Swapper lets you change color definitions.&lt;br /&gt;
Check what your theme will look like in the Showroom display.&lt;br /&gt;
&lt;br /&gt;
When you&apos;re done, use the PullDown Menu to apply the colors to the curent session.</source>
        <translation>I glidebryterinngangsmodus, kan du bruke dem for å senke en del for justering av fargene. I tekstinndatamodus kan du skrive inn fargeverdier direkte. Tilfeldighetsgeneratoren gjør det den sier, og den opprinnelige Jolla-oppførselen er det den ble laget som. Veksleren lar deg endre fargedefinisjoner.&lt;br /&gt;
Sjekk hvordan din drakt vil se ut i visningsrommet.&lt;br /&gt;
&lt;br /&gt;
Når du er ferdig kan du bruke nedtrekksmenyen for å bruke fargene i nåværende økt.</translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="52"/>
        <source>This area allows you to store your created palettes for re-use later. There is one global Cupboard, and one specific for the current Ambience.&lt;br /&gt;
Note that only system-wide Ambiences have a name, custom ones will show as anonymous (for now)</source>
        <translation type="unfinished">Dette området lar deg lagre dine opprettede fargepaletter for gjenbruk senere. Det er et skap for hele systemet, og ett som er spesifikt for omgivelse.&lt;br /&gt;
Merk at kun omgivelser for hele systemet har et navn. Egendefinerte vil vises som anonyme (for øyeblikket).</translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="59"/>
        <source>Jolla Ambiences only define four colors, primary (which is black or white depending on Ambience Scheme), secondary which is a little less opaque primary with some added, plus highlight and secondary highlight which are those selected in the Ambience Settings.&lt;br /&gt;
The App lets you edit other colors than these four, but they will not be affected by Ambience changes or Lipstick restarts as they are stored in the dconf database and stay there. This means once applied through the app, they will always stay the same until you change them again in the App.  You can use the reset option documented below to get rid of them if necessary.
&lt;br /&gt;
The app frequently gets confused about colors when switching editing modes, applying colors to system, or taking palettes from the Cupboard. If that happens, try reloading the colors from the system, that helps in most cases.&lt;br /&gt;
&lt;br /&gt;
It is possible to create color schemes which make parts of the UI unreadable. Check especially non-obvious areas such as the virtual Keyboard.&lt;br /&gt;
It&apos;s a good idea to store a known-good color scheme in the Cupboard so you can go restore easily.&lt;br /&gt;
&lt;br /&gt;
If you have messed up the colors somehow, use the PullUp menu option to reset everything, or from command line issue:&lt;br /&gt;
&lt;pre&gt;dconf dump /desktop/jolla/theme/color/&lt;/pre&gt;
&lt;pre&gt;dconf reset /desktop/jolla/theme/color/highlightBackground&lt;/pre&gt;
and repeat for all the other colors stored there. &lt;br /&gt;
Changing the Ambience from the System Settings may also help.</source>
        <translation type="unfinished">Jolla-omgivelser definerer kun fire farger. «Primær» (som er svart eller hvit avhengig av omgivelsesinnstilling), «Sekundær», har litt mindre dekkevne med litt lagt til, pluss «Framhevet» og «Sekundær framhevelsesfarge» som er de valgt i omgivelsesinnstillingene.
&lt;br /&gt;
Programmet lar deg redigere andre farger enn disse fire, men de blir ikke påvirket av endringer i omgivelse eller Lipstick-omstarter, siden de lagres i dconf-databasen og blir der. Dette betyr at når de er lagt til gjennom programmet, vil de alltid forbli de samme til du endrer dem igjen i programmet. Du kan tilbakestille valget nedenfor for å bli kvitt dem hvis nødvendig.
&lt;br /&gt;
Programmet blir ofte forvirret om farger ved veksling eller redigering av modus, tillegging av farger i systemet, eller å ta fargepaletter fra skapet. Hvis det skjer kan du prøve å gjeninnlaste fargene fra systemet. Det hjelper som oftest.&lt;br /&gt;
&lt;br /&gt;
Det er mulig å skape fargepaletter som gjør deler av grensesnittet uleselig. Sjekk spesielt ikke-åpenbare områder som det virtuelle tastaturet.&lt;br /&gt;
Det er en god idé å lagre farger du vet fungerer i skapet slik at du kan gjenopprette dem enkelt.&lt;br /&gt;
&lt;br /&gt;
Hvis du har tullet til fargene på noe vis, kan du bruke dragingsmenyen for å tilbakestille alt, eller bruke kommandolinjen:&lt;br /&gt;
&lt;pre&gt;dconf dump /desktop/jolla/theme/color/&lt;/pre&gt;
&lt;pre&gt;dconf reset /desktop/jolla/theme/color/highlightBackground&lt;/pre&gt;
og gjenta for alle de andre fargene lagret der.&lt;br /&gt;
Endring av omgivelse fra systeminnstillingene kan også hjelpe.</translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="75"/>
        <source>About</source>
        <translation>Om</translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="76"/>
        <source>Version: </source>
        <translation>Versjon: </translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="77"/>
        <source>Copyright: </source>
        <translation>Opphavsrett: </translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="78"/>
        <source>License: </source>
        <translation>Lisens: </translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="79"/>
        <source>Source Code: </source>
        <translation>Kildekode: </translation>
    </message>
</context>
<context>
    <name>LootBoxItem</name>
    <message>
        <location filename="../qml/components/LootBoxItem.qml" line="21"/>
        <location filename="../qml/components/LootBoxItem.qml" line="26"/>
        <source>ThemeColor</source>
        <translation>DraktFarge</translation>
    </message>
    <message>
        <location filename="../qml/components/LootBoxItem.qml" line="25"/>
        <source>A Lootbox was delivered!</source>
        <translation>En utbyttekasse ble levert.</translation>
    </message>
    <message>
        <location filename="../qml/components/LootBoxItem.qml" line="26"/>
        <source>now has more shelves!</source>
        <translation type="unfinished">har nå flere hyller.</translation>
    </message>
    <message>
        <location filename="../qml/components/LootBoxItem.qml" line="26"/>
        <source>Your persistence has been rewarded.</source>
        <translation>Din standhaftighet har blitt belønnet.</translation>
    </message>
    <message>
        <location filename="../qml/components/LootBoxItem.qml" line="27"/>
        <source>Your persistence has been rewarded!</source>
        <translation>Din standhaftighet har blitt belønnet.</translation>
    </message>
    <message>
        <location filename="../qml/components/LootBoxItem.qml" line="37"/>
        <source>Purchase Options</source>
        <translation>Kjøpsalternativer</translation>
    </message>
    <message>
        <location filename="../qml/components/LootBoxItem.qml" line="46"/>
        <source>Payment* received!</source>
        <translation>Betaling* mottatt.</translation>
    </message>
    <message>
        <location filename="../qml/components/LootBoxItem.qml" line="46"/>
        <source>Buy more shelves</source>
        <translation>Kjøp flere hyller</translation>
    </message>
    <message>
        <location filename="../qml/components/LootBoxItem.qml" line="51"/>
        <source>Using Jolla Shop credentials to buy Storage Lootbox</source>
        <translation>Bruk identitetsdetaljene fra Jolla-butikken for å kjøpe lagringsutbyttekasse</translation>
    </message>
    <message>
        <location filename="../qml/components/LootBoxItem.qml" line="64"/>
        <source>Thank you for your purchase!&lt;br /&gt;Your extra shelves will be delivered in the next update!</source>
        <translation>Takk for kjøpet.&lt;br /&gt;Dine ekstra hyller vil bli levert i neste oppdatering.</translation>
    </message>
    <message>
        <location filename="../qml/components/LootBoxItem.qml" line="74"/>
        <source>*) No seriously there are no in-app-purchases in this app or Lootboxes in the Jolla Shop. That would be ridiculous.&lt;br /&gt;No funds were transfered.&lt;br /&gt; In fact, nothing really happened just now.&lt;br /&gt;…or did it?</source>
        <translation type="unfinished">*) Neida, det er ingen kjøpselementer i dette programmet eller utbyttekasser i Jolla-butikken. Det skulle tatt seg ut.&lt;br /&gt;Ingen penger ble oveført.&lt;br /&gt;Det skjedde faktisk ingenting nå.&lt;br /&gt;…eller?</translation>
    </message>
    <message>
        <source>*) No seriously there are no in-app-purchases in this app or Lootboxes in the Jolla Shop. That would be ridiculous.&lt;br /&gt;No funds were transfered.&lt;br /&gt; In fact, nothing really happened just now.&lt;br /&gt;...or did it?</source>
        <translation type="vanished">*) No seriously there are no in-app-purchases in this app or Lootboxes in the Jolla Shop. That would be ridiculous.&lt;br /&gt;No funds were transfered.&lt;br /&gt; In fact, nothing really happened just now.&lt;br /&gt;...or did it?</translation>
    </message>
</context>
<context>
    <name>SaveSlot</name>
    <message>
        <location filename="../qml/components/SaveSlot.qml" line="39"/>
        <source>Ambience</source>
        <translation type="unfinished">Omgivelse</translation>
    </message>
    <message>
        <location filename="../qml/components/SaveSlot.qml" line="39"/>
        <source>Shelf</source>
        <translation>Hylle</translation>
    </message>
    <message>
        <location filename="../qml/components/SaveSlot.qml" line="63"/>
        <source>Take to Lab</source>
        <translation type="unfinished">Ta til laboratoriet</translation>
    </message>
    <message>
        <location filename="../qml/components/SaveSlot.qml" line="64"/>
        <source>Put on Shelf</source>
        <translation>Putt på hyllen</translation>
    </message>
</context>
<context>
    <name>Saver</name>
    <message>
        <location filename="../qml/pages/Saver.qml" line="71"/>
        <source>Global Cupboard</source>
        <translation type="unfinished">Skap for hele systemet</translation>
    </message>
    <message>
        <location filename="../qml/pages/Saver.qml" line="104"/>
        <source>Clean out this cupboard</source>
        <translation>Tøm dette skapet</translation>
    </message>
    <message>
        <location filename="../qml/pages/Saver.qml" line="104"/>
        <source>Spring Clean</source>
        <translation>Vårrengjøring</translation>
    </message>
</context>
<context>
    <name>SaverPlus</name>
    <message>
        <location filename="../qml/pages/SaverPlus.qml" line="11"/>
        <source>anonymous</source>
        <translation>anonym</translation>
    </message>
    <message>
        <location filename="../qml/pages/SaverPlus.qml" line="103"/>
        <source>Ambience Cupboard</source>
        <translation type="unfinished">Omgivelsesskap</translation>
    </message>
    <message>
        <location filename="../qml/pages/SaverPlus.qml" line="147"/>
        <source>Clean out this cupboard</source>
        <translation>Tøm dette skapet</translation>
    </message>
    <message>
        <location filename="../qml/pages/SaverPlus.qml" line="147"/>
        <source>Spring Clean</source>
        <translation>Vårrengjøring</translation>
    </message>
</context>
<context>
    <name>ShowRoom</name>
    <message>
        <location filename="../qml/components/ShowRoom.qml" line="89"/>
        <location filename="../qml/components/ShowRoom.qml" line="90"/>
        <location filename="../qml/components/ShowRoom.qml" line="91"/>
        <location filename="../qml/components/ShowRoom.qml" line="92"/>
        <location filename="../qml/components/ShowRoom.qml" line="93"/>
        <source>A very long line showing Text in </source>
        <translation>En veldig lang tekstlinje som viser tekst i </translation>
    </message>
    <message>
        <source>Color</source>
        <translation type="vanished">Colour</translation>
    </message>
    <message>
        <location filename="../qml/components/ShowRoom.qml" line="89"/>
        <location filename="../qml/components/ShowRoom.qml" line="108"/>
        <location filename="../qml/components/ShowRoom.qml" line="136"/>
        <source>Primary Color</source>
        <translation>Primærfarge</translation>
    </message>
    <message>
        <location filename="../qml/components/ShowRoom.qml" line="90"/>
        <location filename="../qml/components/ShowRoom.qml" line="114"/>
        <location filename="../qml/components/ShowRoom.qml" line="142"/>
        <source>Secondary Color</source>
        <translation>Sekundærfarge</translation>
    </message>
    <message>
        <location filename="../qml/components/ShowRoom.qml" line="91"/>
        <source>Highlight Color</source>
        <translation>Framhevelsesfarge</translation>
    </message>
    <message>
        <location filename="../qml/components/ShowRoom.qml" line="92"/>
        <source>Secondary Highlight Color</source>
        <translation>Sekundær framhevelsesfarge</translation>
    </message>
    <message>
        <location filename="../qml/components/ShowRoom.qml" line="93"/>
        <source>Error Color</source>
        <translation>Feilfarge</translation>
    </message>
    <message>
        <location filename="../qml/components/ShowRoom.qml" line="108"/>
        <location filename="../qml/components/ShowRoom.qml" line="114"/>
        <location filename="../qml/components/ShowRoom.qml" line="136"/>
        <location filename="../qml/components/ShowRoom.qml" line="142"/>
        <source>Background Color</source>
        <translation>Bakgrunnsfarge</translation>
    </message>
    <message>
        <source>four kinds of background overlay opacities and colors</source>
        <translation type="vanished">four kinds of background overlay opacities and colours</translation>
    </message>
    <message>
        <location filename="../qml/components/ShowRoom.qml" line="108"/>
        <location filename="../qml/components/ShowRoom.qml" line="114"/>
        <location filename="../qml/components/ShowRoom.qml" line="136"/>
        <location filename="../qml/components/ShowRoom.qml" line="142"/>
        <source>Text</source>
        <translation>Tekst</translation>
    </message>
    <message>
        <location filename="../qml/components/ShowRoom.qml" line="165"/>
        <source>Progress Bar Demo</source>
        <translation>Framdriftsbjelke-demo</translation>
    </message>
    <message>
        <location filename="../qml/components/ShowRoom.qml" line="165"/>
        <source>Tap to restart Demos</source>
        <translation>Trykk for å tilbakestille demoer</translation>
    </message>
    <message>
        <location filename="../qml/components/ShowRoom.qml" line="168"/>
        <source>Remorse Item Demo</source>
        <translation type="unfinished">Angringselement-demo</translation>
    </message>
    <message>
        <location filename="../qml/components/ShowRoom.qml" line="252"/>
        <location filename="../qml/components/ShowRoom.qml" line="253"/>
        <source>Button</source>
        <translation>Knapp</translation>
    </message>
</context>
</TS>
