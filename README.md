# Theme Color

Editor for the SailfishOS theme colors.

See: https://openrepos.net/content/nephros/theme-color

Translations are welcome via [Weblate](https://hosted.weblate.org/projects/theme-color/).
