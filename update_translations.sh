#!/bin/sh

BASENAMES=openrepos-themecolor
LANGS="en_IE de nb_NO zh_CN"

#RELEASE_OPT = -nounfinished -silent -compress -idbased -markuntranslated !
# do not use -compress, this does not work!
#RELEASE_OPT = -idbased
RELEASE_OPT="-nounfinished -removeidentical"

for f in "${BASENAMES}"; do
  for l in ${LANGS}; do
    lupdate -recursive qml -target-language "$l" -ts translations/"${f}"-"${l}".ts 
    lrelease ${RELEASE_OPT} translations/"${f}"-"${l}".ts
  done
  sed 's/en_IE/en_GB/' translations/"${f}-en_IE.ts" >  translations/"${f}-en_GB.ts"
  lrelease ${RELEASE_OPT} translations/"${f}-en_GB.ts"
done
