import QtQuick 2.6
import Sailfish.Silica 1.0
import "../components"

Page {
  id: page

  // map dropdown selection to ints
  readonly property var inputMode: QtObject {
    property int sliders: 0
    property int text: 1
    property int swapper: 2
    property int random: 3
    property int jolla: 4
    property int defaultValue: this.sliders
    }

  Component.onCompleted: { initColors() }
  onStatusChanged: {
    if ( status === PageStatus.Active ) { pageStack.pushAttached(Qt.resolvedUrl("Saver.qml")) }
  }

  allowedOrientations: (devicemodel === 'planetgemini') ?  Orientation.LandscapeInverted : defaultAllowedOrientations

  SilicaFlickable {
    id: flick
    contentHeight: col.height
    anchors.fill: parent
    VerticalScrollDecorator {}
    Column {
        id: col
        width: parent.width
        spacing: Theme.paddingLarge
        //populate: Transition { NumberAnimation { property: "opacity"; from: 0; to: 1.0; duration: 240 } }
        add: Transition { NumberAnimation { property: "opacity"; from: 0; to: 1.0; duration: 240 } }
        move: Transition { NumberAnimation { properties: "x,y"; duration: 240; easing.type: Easing.OutQuad } }
        PageHeader { id: head ; title: qsTr("Adjust Theme Colors") }
        SectionHeader {
          id: header; text: qsTr("Showroom"); font.pixelSize: Theme.fontSizeLarge
          color: showroom.visible ? Theme.highlightColor : Theme.secondaryColor
          Behavior on color { ColorAnimation { } }
          BackgroundItem { anchors.fill: parent; onClicked: { showroom.visible = ! showroom.visible } }
        }
        ShowRoom {
          id: showroom
          width: parent.width
          anchors.horizontalCenter: parent.horizontalCenter
        }
        Separator {
          width: parent.width
          height: 2
          horizontalAlignment: showroom.visible ? Qt.AlignRight : Qt.AlignLeft
          color: Theme.highlightColor
        }
        SectionHeader {
          text: qsTr("Laboratory"); font.pixelSize: Theme.fontSizeLarge
          color: ( modeSelector.visible && selectors.visible ) ? Theme.highlightColor : Theme.secondaryColor
          Behavior on color { ColorAnimation { } }
          BackgroundItem { anchors.fill: parent;
            onClicked: {
                    modeSelector.visible = ! modeSelector.visible;
                    selectors.visible = ! selectors.visible;
                    }
          }
        }
        ComboBox {
          id: modeSelector
          anchors.horizontalCenter: parent.horizontalCenter
          label: qsTr("Input Mode" + ":")
          description: qsTr("Tap to switch")
          currentIndex: inputMode.defaultValue
          menu: ContextMenu {
            MenuItem { text: qsTr("Sliders") }
            MenuItem { text: qsTr("Text") }
            MenuItem { text: qsTr("Swapper/Copier") }
            MenuItem { text: qsTr("Randomizer") }
            MenuItem { text: qsTr("Jolla Original") }
          }
          onValueChanged: {
            if ( (currentIndex !== inputMode.sliders) && (currentIndex !== inputMode.text) ) {
                flick.scrollToBottom();
            }
         }
        }
        Column {
          id: selectors
          anchors.horizontalCenter: parent.horizontalCenter
          width: parent.width
          ColorSliders {
              visible: modeSelector.currentIndex === inputMode.sliders
          }
          ColorTextInputs {
              visible: modeSelector.currentIndex === inputMode.text
          }
          ColorSwapper {
              anchors.horizontalCenter: parent.horizontalCenter
              anchors.bottomMargin: Theme.itemSizeMedium
              visible: modeSelector.currentIndex === inputMode.swapper
          }
          ColorSwapper {
              anchors.horizontalCenter: parent.horizontalCenter
              anchors.bottomMargin: Theme.itemSizeMedium
              visible: modeSelector.currentIndex === inputMode.swapper
              copy: true
          }
          ColorRandomizer {
              anchors.horizontalCenter: parent.horizontalCenter
              anchors.bottomMargin: Theme.itemSizeMedium
              visible: modeSelector.currentIndex === inputMode.random
          }
          CollaSlider {
              anchors.horizontalCenter: parent.horizontalCenter
              anchors.bottomMargin: Theme.itemSizeMedium
              visible: modeSelector.currentIndex === inputMode.jolla
          }
        }
        Separator {
          width: parent.width
          height: 2
          horizontalAlignment: selectors.visible ? Qt.AlignHCenter : Qt.AlignLeft
          color: Theme.highlightColor
        }
    }
    PullDownMenu {
        MenuItem { text: qsTr("Help");
                   onClicked: { pageStack.push(Qt.resolvedUrl("HelpPage.qml")) }
                 }
        MenuItem { text: qsTr("Open Ambience Settings");
                   onClicked: { settings.open() }
                 }
        MenuItem { text: qsTr("Compute all Colors from Highlight");
                   onClicked: { applyRemorse.execute(qsTr("Applying") + "…", function () { computeColors() } ) }
                 }
        MenuItem { text: qsTr("Apply Colors to System");
                   onClicked: { applyRemorse.execute(qsTr("Applying") + "…", function () { applyThemeColors() } ) }
                 }
        MenuItem { text: qsTr("Reload Colors from current Theme");
                   onClicked: { reloadThemeColors() }
                 }
        MenuItem { text: qsTr("Reload Colors from System Config");
                   onClicked: { colorsInitialized = false; initColors() }
                 }
    }
    PushUpMenu {
      quickSelect: false
        MenuLabel { text: qsTr("Experimental or dangerous actions") }
        MenuItem  { text: qsTr("Export to Ambience file") + " " + qsTr("(not implemented)");
                    onClicked: { ambienceExport() }
                    enabled: false;
                  }
        MenuItem  { text: qsTr("Save Theme to current Ambience") + " " + qsTr("(not implemented)");
                    onClicked: { applyRemorse.execute( qsTr("Saving") + "…", function () { saveAmbience() } ) }
                    enabled: false;
                  }
        MenuItem  { text: qsTr("Reset all values and restart");
                    onClicked: { applyRemorse.execute( qsTr("Resetting") + "…", function () { resetDconf(); restartLipstick(); } ) }
                  }
        MenuItem  { text: qsTr("Reset nonstandard values");
                    onClicked: { applyRemorse.execute( qsTr("Resetting") + "…", function () { resetDconf(); } ) }
                  }
        MenuItem  { text: qsTr("Restart Lipstick");
                    onClicked: { applyRemorse.execute( qsTr("Restarting") + "…", function () { restartLipstick() } ) }
                  }
    }

    RemorsePopup {
      id: applyRemorse
    }
  }
}

// vim: expandtab ts=4 st=4
