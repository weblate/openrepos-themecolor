pragma Singleton
import QtQuick 2.6

QtObject {
  readonly property var version: 0
  readonly property var release: 0
  default property string versionstring: version + "-" + release
  readonly property string copyright: "2021 Peter G. (nephros) <" + this.email + ">"
  readonly property string email: "mailto:sailfish@nephros.org"
  readonly property string license: "MIT License"
  readonly property string licenseurl: "https://opensource.org/licenses/MIT"
  readonly property string source: "https://gitlab.com/nephros/openrepos-themecolor/"
}

// vim: expandtab ts=4 st=4
