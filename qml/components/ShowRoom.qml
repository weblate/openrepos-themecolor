import QtQuick 2.6
import Sailfish.Silica 1.0
import "../components"

SilicaItem {
    id: showroom
    highlighted: false
    height: header.height + showcol.height

    readonly property int maxcounter: 20
    property int counter: maxcounter

    Timer {
      id: timer
      interval: 500
      repeat: true
      running: ( counter > 0 && parent.visible )
      triggeredOnStart: true
      onTriggered: { counter -= 1 }
    }

    Image {
        id: showbg
        anchors.top: showcol.top
        cache: true
        source: "file://" + imagepath.value
        width: parent.width - Theme.paddingSmall
        height: showcol.height
        scale: 0.8
        opacity: 1.0
        fillMode: Image.PreserveAspectFit
    }
    Rectangle {
        id: firstrec
        anchors.verticalCenter: showbg.verticalCenter
        anchors.left: showbg.left
        height: showbg.height
        width: showbg.width / 5
        color: MyPalette.overlayBackgroundColor
        opacity: Theme.opacityHigh
        radius: Theme.paddingLarge
    }
    Rectangle {
        id: secrec
        anchors.verticalCenter: showbg.verticalCenter
        anchors.left: firstrec.right
        height: showbg.height
        width: firstrec.width
        color: MyPalette.overlayBackgroundColor
        opacity: Theme.opacityLow
        radius: Theme.paddingLarge
    }
    Rectangle {
        id: trdrec
        anchors.verticalCenter: showbg.verticalCenter
        anchors.left: secrec.right
        height: showbg.height
        width: secrec.width
        color: MyPalette.overlayBackgroundColor
        opacity: Theme.opacityFaint
        radius: Theme.paddingLarge
    }
    Rectangle {
        id: fourthrec
        anchors.verticalCenter: showbg.verticalCenter
        anchors.left: trdrec.right
        height: showbg.height
        width: trdrec.width
        color: MyPalette.highlightBackgroundColor
        opacity: Theme.highlightBackgroundOpacity
        radius: Theme.paddingLarge
    }
    Column {
        id: showcol
        width: parent.width
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: parent.top
        spacing: Theme.paddingMedium/*}}}*/

        /* ************* All the Text *****************/
        SilicaItem {
            anchors.horizontalCenter: parent.horizontalCenter
            width: parent.width
            height: lblcol0.height
            Column {
              id: lblcol0
              width: parent.width
              spacing: Theme.paddingSmall
              Label { anchors.horizontalCenter: parent.horizontalCenter; horizontalAlignment: Text.AlignHCenter ; text: qsTr("A very long line showing Text in ") + " " + qsTr("Primary Color");   color: MyPalette.primaryColor }
              Label { anchors.horizontalCenter: parent.horizontalCenter; horizontalAlignment: Text.AlignHCenter ; text: qsTr("A very long line showing Text in ") + " " + qsTr("Secondary Color"); color: MyPalette.secondaryColor }
              Label { anchors.horizontalCenter: parent.horizontalCenter; horizontalAlignment: Text.AlignHCenter ; text: qsTr("A very long line showing Text in ") + " " + qsTr("Highlight Color"); color: MyPalette.highlightColor }
              Label { anchors.horizontalCenter: parent.horizontalCenter; horizontalAlignment: Text.AlignHCenter ; text: qsTr("A very long line showing Text in ") + " " + qsTr("Secondary Highlight Color");   color: MyPalette.secondaryHighlightColor }
              Label { anchors.horizontalCenter: parent.horizontalCenter; horizontalAlignment: Text.AlignHCenter ; text: qsTr("A very long line showing Text in ") + " " + qsTr("Error Color");     color: MyPalette.errorColor}
            }
        }
        /* ************* Background Colors *****************/
        SilicaItem {
            anchors.horizontalCenter: parent.horizontalCenter
            width: parent.width
            height: lblcol1.height
            Column {
              id: lblcol1
              width: parent.width
              spacing: Theme.paddingSmall
              Label {
                anchors.horizontalCenter: parent.horizontalCenter
                horizontalAlignment: Text.AlignHCenter
                text: qsTr("Primary Color") + " " + qsTr("Text") + ", highlight " + qsTr("Background Color")
                color: MyPalette.primaryColor
              }
              Label {
                anchors.horizontalCenter: parent.horizontalCenter
                horizontalAlignment: Text.AlignHCenter
                text: qsTr("Secondary Color") + " " + qsTr("Text") + ", highlight " + qsTr("Background Color")
                color: MyPalette.secondaryColor
                font.pixelSize: Theme.fontSizeSmall
              }
            }
            Separator {
              horizontalAlignment: Qt.AlignHCenter
              color: MyPalette.highlightBackgroundColor
              anchors.fill: parent
            }
        }
        SilicaItem {
            anchors.horizontalCenter: parent.horizontalCenter
            width: parent.width
            height: lblcol2.height
            Column {
              id: lblcol2
              width: parent.width
              spacing: Theme.paddingSmall
              Label {
                anchors.horizontalCenter: parent.horizontalCenter
                horizontalAlignment: Text.AlignHCenter
                text: qsTr("Primary Color") + " " + qsTr("Text") + ", overlay " + qsTr("Background Color")
                color: MyPalette.primaryColor
              }
              Label {
                anchors.horizontalCenter: parent.horizontalCenter
                horizontalAlignment: Text.AlignHCenter
                text: qsTr("Secondary Color") + " " + qsTr("Text") + ", overlay " + qsTr("Background Color")
                color: MyPalette.secondaryColor
                font.pixelSize: Theme.fontSizeSmall
              }
            }
            Separator {
              horizontalAlignment: Qt.AlignHCenter
              color: MyPalette.overlayBackgroundColor
              anchors.fill: parent
            }
        }
        /* ************* Progress and Remorse Simulator *****************/
        SilicaItem {
            id: pbitem
            width: parent.width
            height: pb.height
            anchors.horizontalCenter: parent.horizontalCenter;
            ProgressBar {
                width: parent.width
                id: pb
                value: counter
                minimumValue: 0
                maximumValue: maxcounter
                label: value > minimumValue ? qsTr("Progress Bar Demo") : qsTr("Tap to restart Demos")
                onValueChanged: {
                  if ( value === 0 ) {
                    remorse.execute(pb, qsTr("Remorse Item Demo") + "…", function() { counter = -1 } )
                  }
                }
                BackgroundItem {
                  anchors.fill: parent
                  onClicked: { counter = maxcounter }
                }
            }
            RemorseItem { id: remorse; }
        }
        /* ************* TopMenu Simulator *****************/
        SilicaItem {
            width: brow.width
            height: tbrow.height + Theme.paddingLarge * 2
            anchors.horizontalCenter: parent.horizontalCenter;
            Rectangle {
              z: -1
              anchors.fill: parent
              anchors.centerIn: parent
              anchors.horizontalCenter: parent.horizontalCenter;
              color: MyPalette.overlayBackgroundColor
              opacity: Theme.opacityOverlay
            }
            Row {
              id: tbrow
              anchors.horizontalCenter: parent.horizontalCenter
              anchors.verticalCenter: parent.verticalCenter;
              //height: Theme.iconSizeMedium
              spacing: Theme.iconSizeMedium
              ColorIndicator3 { iconcolor: MyPalette.highlightColor;   icon: "image://theme/icon-m-wlan-2" }
              ColorIndicator3 { iconcolor: MyPalette.primaryColor; icon: "image://theme/icon-m-location" }
              ColorIndicator3 { iconcolor: MyPalette.highlightColor; icon: "image://theme/icon-m-bluetooth"; }
              ColorIndicator3 { iconcolor: MyPalette.primaryColor; icon: "image://theme/icon-m-ambience"; }
            }
        }
        /* ************* Pulley Menu Simulator *****************/
        SilicaItem {
           width: brow.width
           height: gicol.height + Theme.paddingLarge * 2
           anchors.horizontalCenter: parent.horizontalCenter;
           Rectangle {
             id: girec
             anchors.fill: gicol
             height: gicol.height
             color: MyPalette.highlightBackgroundColor
             gradient: Gradient {
               GradientStop { position: 0.0; color: Theme.rgba(MyPalette.highlightBackgroundColor, Theme.highlightBackgroundOpacity) }
               GradientStop { position: 0.5; color: Theme.rgba(MyPalette.highlightBackgroundColor, Theme.highlightBackgroundOpacity) }
               GradientStop { position: 1.0; color: Theme.rgba(MyPalette.highlightBackgroundColor, 2*Theme.highlightBackgroundOpacity) }
             }
           }
           Column {
             id: gicol
             spacing: 0
             width: parent.width
             //MenuLabel { text: "PullDownMenu";   height: Theme.itemSizeExtraSmall; anchors.topMargin: Theme.itemSizeTiny; anchors.bottomMargin: Theme.itemSizeTiny}
             MenuItem { text: qsTr("MenuItem");              color: MyPalette.highlightColor; height: Theme.itemSizeExtraSmall; highlighted: true}
             MenuItem { text: qsTr("MenuItem") + " " + qsTr("selected");          color: MyPalette.primaryColor; height: Theme.itemSizeExtraSmall; down: false; highlighted: false
               HighlightBar { highlightedItem: parent;
               color: MyPalette.highlightColor
               anchors.horizontalCenter: parent.horizontalCenter;
               anchors.verticalCenter: parent.verticalCenter;
               }
             }
             MenuItem { text: qsTr("MenuItem") + " " + qsTr("disabled"); height: Theme.itemSizeTiny; enabled: false; }
             MenuItem { text: ""; height: Theme.itemSizeTiny; enabled: false; }
           }
           GlassItem {
             width: parent.width
             height: Theme.paddingLarge
             anchors.horizontalCenter: parent.horizontalCenter;
             anchors.verticalCenter: gicol.bottom
             anchors.verticalCenterOffset: -gicol.spacing
             radius: 0.35
             color: MyPalette.highlightBackgroundColor
             opacity: 1.0
             falloffRadius:  0.2
             brightness: 1.0
           }
        }
        /* ************* Buttons *****************/
        Row {
          id: brow
          anchors.horizontalCenter: parent.horizontalCenter;
          Button { text: qsTr("Button");               width: Theme.buttonWidthSmall; color: MyPalette.primaryColor }
          Button { text: qsTr("Button"); down: false;  width: Theme.buttonWidthSmall; color: MyPalette.primaryColor }
        }
        Row {
          id: ibrow
          anchors.horizontalCenter: parent.horizontalCenter;
          ColorIndicator1 { busy: timer.running; iconcolor: MyPalette.primaryColor}
          ColorIndicator1 { checked: false; iconcolor: MyPalette.primaryColor }
          ColorIndicator1 { checked: true;  iconcolor: MyPalette.secondaryColor }
          ColorIndicator1 { checked: false; iconcolor: MyPalette.highlightColor }
          ColorIndicator1 { checked: true;  iconcolor: MyPalette.secondaryHighlightColor }
        }
    }
}

// vim: expandtab ts=4 softtabstop=4
