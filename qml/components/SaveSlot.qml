import QtQuick 2.6
import Sailfish.Silica 1.0
//import "../components"
import "."

Column {
    property bool ambience : false
    property int idx

    property color p
    property color s
    property color h
    property color sh
    property color hbg
    property color bgg

    function initThemeCols() {
        p = getThemeProp(idx, "p");
        s = getThemeProp(idx, "s");
        h = getThemeProp(idx, "h");
        sh = getThemeProp(idx, "sh");
        hbg = getThemeProp(idx, "hbg");
        bgg = getThemeProp(idx, "bgg");
    }

    anchors.horizontalCenter: parent.horizontalCenter
    // incorrect for a delegate in ListView:
    //width: parent.width - Theme.itemSizeLarge * 2
    spacing: Theme.paddingMedium

    // onCompleted doesn't suffice for a delegate...
    onVisibleChanged: {
      //console.log("instatiating slot with index " + idx)
      initThemeCols()
    }

    SectionHeader {
      width: parent.width
      text: ( ambience === true ) ? qsTr("Ambience") + " " + qsTr("Shelf") + " " + (idx + 1) : qsTr("Shelf") + " " + ( idx + 1)
      font.pixelSize: Theme.fontSizeMedium
      color: Theme.secondaryHighlightColor
    }
    Separator {
      anchors.horizontalCenter: parent.horizontalCenter
      horizontalAlignment : Qt.AlignHCenter
      width: parent.width
      color: Theme.rgba(Theme.highlightBackgroundColor, Theme.opacityFaint)
    }
    Row {
        id: ibrow
        anchors.horizontalCenter: parent.horizontalCenter;
        ColorJar { col: p  }
        ColorJar { col: s  }
        ColorJar { col: h  }
        ColorJar { col: sh }
        ColorJar { col: ( Theme.colorScheme == Theme.LightOnDark ) ? "white" : "black"; bgcol: hbg }
        GlowIndicator { backgroundColor: bgg }
    }
    Row {
      id: btnrow
      anchors.horizontalCenter: parent.horizontalCenter
      spacing: Theme.paddingLarge * 2
      Button { text: qsTr("Take to Lab");  width: Theme.buttonSizeSmall; height: Theme.iconSizeSmall; onClicked: { console.debug("loading " + idx + "…") ; loadTheme(idx) } }
      Button { text: qsTr("Put on Shelf"); width: Theme.buttonSizeSmall; height: Theme.iconSizeSmall; onClicked: { console.debug("saving " + idx + "…")  ; saveTheme(idx) ; initThemeCols() } }
    }
    Separator {
      anchors.horizontalCenter: parent.horizontalCenter
      anchors.topMargin: Theme.paddingLarge
      horizontalAlignment : Qt.AlignHCenter
      width: parent.width
      height: Theme.paddingSmall
      color: Theme.rgba(Theme.highlightBackgroundColor, Theme.opacityFaint)
    }
}


// vim: expandtab ts=4 st=4
