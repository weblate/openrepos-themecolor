import QtQuick 2.6
import Sailfish.Silica 1.0

SilicaItem {
  property string icon
  property color iconcolor
  //property bool highlighted: false
  height: Theme.iconSizeMedium
  width:  Theme.iconSizeMedium
  Icon {
    id: iconbg
    source: "image://theme/icon-s-clear-opaque-background?" + parent.iconcolor
    opacity: Theme.opacityLow
    height: parent.height
    width: parent.height 
    anchors.verticalCenter: parent.verticalCenter
  }
  Icon {
    anchors.centerIn: iconbg
    source: parent.icon
    color: parent.iconcolor
    opacity: 1.0
    height: parent.height * (2/3)
    width: parent.width *  (2/3)
  }
}

// vim: expandtab ts=4 st=4
