import QtQuick 2.6
import Sailfish.Silica 1.0

Column {
    property color incolor
    property var outcolor: incolor

    Component {
      id: tiDialogPage
      Dialog {
        id: tiDialog
        property string coltext
        canAccept: tiField.acceptableInput && !tiField.focus
        width: parent.width
        Column {
          width: parent.width
          DialogHeader {}
          Row {
            anchors.horizontalCenter: parent.horizontalCenter
            ColorIndicator2 {
              anchors.verticalCenter: tiField.verticalCenter
              iconcolor: tiField.acceptableInput ? tiField.text : "transparent"
              opacity:  tiField.acceptableInput ? 1.0 : Theme.opacityLow
              Behavior on opacity { FadeAnimator {} }
            }
            ColorField {
              id: tiField
              width: Theme.buttonWidthLarge
              //width:parent.width * ( 2 / 3)
              col: incolor;
              name: qsTr("Color input")
              text: outcolor
              EnterKey.enabled: acceptableInput
              EnterKey.onClicked: {
                  focus = false
                  tiDialog.accept()
                }
            }
        }
        }
        onDone: {
          if (result == DialogResult.Accepted) {
            var startsWith = /^#/;
            if ( ! startsWith.test(tiField.text) ) {
                coltext = tiField.text.replace(/^/, "#")
            } else {
                coltext = tiField.text
            }
            console.debug("returned from dialog: " + tiField.text )
          }
        }
      }
    }

    width: page.isLandscape ? parent.width - Theme.itemSizeLarge * 2 : parent.width
    anchors.horizontalCenter: parent.horizontalCenter
    spacing: 0
    Row {
      anchors.horizontalCenter: parent.horizontalCenter
      width: btnrow.width
      spacing:0
      ColRect { width: parent.width / 2; color: incolor }
      ColRect { width: parent.width / 2; color: outcolor }
    }
    Row {
      id: btnrow
      width: parent.width
      anchors.horizontalCenter: parent.horizontalCenter
      spacing: 0
      ValueButton {
         id: valbutton
         width: parent.width * ( 2 / 4 )
         description: qsTr("Input value, tap to reset")
         label: ( incolor.a < 1 ) ? "(a)RGB" : "RGB"
         value: outcolor
         valueColor: Theme.secondaryColor
         labelColor: Theme.secondaryColor
         onClicked: { outcolor = incolor }
      }
      IconButton {
        id: tibutton
        anchors.verticalCenter: valbutton.verticalCenter
        height: valbutton.height
        width: parent.width * ( 1/ 4 )
        icon.source: "image://theme/icon-m-text-input?" + Theme.highlightColor
        onClicked: {
          var dialog = pageStack.push(tiDialogPage)
          dialog.accepted.connect(function() { outcolor = dialog.coltext })
        }
       }
      IconButton {
        id: pickbutton
        anchors.verticalCenter: valbutton.verticalCenter
        height: valbutton.height
        width: parent.width * ( 1 / 4 )
        icon.source: "image://theme/icon-m-wizard?" + Theme.highlightColor
        onClicked: {
          var dialog = pageStack.push("Sailfish.Silica.ColorPickerDialog")
          dialog.accepted.connect(function() { outcolor = dialog.color })
        }
      }
    }
}

// vim: expandtab ts=4 st=4
