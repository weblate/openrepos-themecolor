import QtQuick 2.6
import QtQml.Models 2.1
import Sailfish.Silica 1.0
import "."

GlassItem {
    height: Theme.iconSizeMedium
    width: Theme.iconSizeMedium
    color: MyPalette.primaryColor
    backgroundColor: MyPalette.backgroundGlowColor
    radius: 0.22
    falloffRadius: 0.18
}

// vim: expandtab ts=4 st=4
