import QtQuick 2.6
import Sailfish.Silica 1.0
import "../components"

Row {
    RemorsePopup {
      id: rndRemorse
        onCanceled: {
            colorsInitialized = false
            initColors()
        }
    }


    anchors.horizontalCenter: parent.horizontalCenter
    anchors.bottomMargin: Theme.itemSizeMedium
    Button {
      id: rndbtn
      preferredWidth: page.isLandscape ? Theme.buttonWidthLarge : parent.width * (1 / 4)
      text:  qsTr("Random") + " " + qsTr("Colors")
      color: Qt.rgba(Math.random(), Math.random(), Math.random(), MyPalette.primaryColor.a);
      onClicked: {
        MyPalette.primaryColor            = Qt.rgba(Math.random(), Math.random(), Math.random(), MyPalette.primaryColor.a);
        MyPalette.secondaryColor          = Qt.rgba(Math.random(), Math.random(), Math.random(), MyPalette.secondaryColor.a);
        MyPalette.highlightColor          = Qt.rgba(Math.random(), Math.random(), Math.random(), MyPalette.highlightColor.a);
        MyPalette.secondaryHighlightColor = Qt.rgba(Math.random(), Math.random(), Math.random(), MyPalette.secondaryHighlightColor.a);
        MyPalette.highlightBackgroundColor = Theme.highlightBackgroundFromColor( MyPalette.highlightColor, MyPalette.colorScheme );
        MyPalette.highlightDimmerColor = Theme.highlightDimmerFromColor( MyPalette.highlightColor, MyPalette.colorScheme );
        color = Qt.rgba(Math.random(), Math.random(), Math.random(), MyPalette.primaryColor.a);
        rndRemorse.execute( qsTr("Generated") +  " " + text + "…",   function () { }, 2400 )
      }
    }
    Button {
      preferredWidth: rndbtn.width
      text: ( Theme.colorScheme == Theme.LightOnDark ) ? qsTr("Bright") + " " + qsTr("Colors") : qsTr("Dark") + " " + qsTr("Colors")
      color: {
        var cmax = ( Theme.colorScheme == Theme.LightOnDark ) ? 1.0 : 0.7;
        var cmin = ( Theme.colorScheme == Theme.LightOnDark ) ? 0.3 : 0.0;
        Qt.rgba(Math.random() * (cmax - cmin) + cmin, Math.random() * (cmax - cmin) + cmin, Math.random() * (cmax - cmin) + cmin, MyPalette.primaryColor.a);
      }
      onClicked: {
        var cmax = ( Theme.colorScheme == Theme.LightOnDark ) ? 1.0 : 0.7;
        var cmin = ( Theme.colorScheme == Theme.LightOnDark ) ? 0.3 : 0.0;
        MyPalette.primaryColor            = Qt.rgba(Math.random() * (cmax - cmin) + cmin, Math.random() * (cmax - cmin) + cmin, Math.random() * (cmax - cmin) + cmin, MyPalette.primaryColor.a);
        MyPalette.secondaryColor          = Qt.rgba(Math.random() * (cmax - cmin) + cmin, Math.random() * (cmax - cmin) + cmin, Math.random() * (cmax - cmin) + cmin, MyPalette.secondaryColor.a);
        MyPalette.highlightColor          = Qt.rgba(Math.random() * (cmax - cmin) + cmin, Math.random() * (cmax - cmin) + cmin, Math.random() * (cmax - cmin) + cmin, MyPalette.highlightColor.a);
        MyPalette.secondaryHighlightColor = Qt.rgba(Math.random() * (cmax - cmin) + cmin, Math.random() * (cmax - cmin) + cmin, Math.random() * (cmax - cmin) + cmin, MyPalette.secondaryHighlightColor.a);
        MyPalette.highlightBackgroundColor = Theme.highlightBackgroundFromColor( MyPalette.highlightColor, MyPalette.colorScheme );
        MyPalette.highlightDimmerColor = Theme.highlightDimmerFromColor( MyPalette.highlightColor, MyPalette.colorScheme );
        color = Theme.highlightFromColor( MyPalette.primaryColor, Theme.colorScheme);
        rndRemorse.execute( qsTr("Generated") +  " " + text + "…",   function () { }, 2400 )
      }
    }
    Button {
      preferredWidth: rndbtn.width
      text:  qsTr("Gray") + " " + qsTr("Colors")
      color: ( Theme.colorScheme == Theme.LightOnDark ) ? Theme.highlightFromColor("lightgray", Theme.colorScheme) : Theme.highlightFromColor("dimgray", Theme.colorScheme)
      onClicked: {
        var cmax = ( Theme.colorScheme == Theme.LightOnDark ) ? 1.0 : 0.4;
        var cmin = ( Theme.colorScheme == Theme.LightOnDark ) ? 0.6 : 0.0;
        var rp = Math.random() * (cmax - cmin) + cmin
        var rs = Math.random() * (cmax - cmin) + cmin
        var rh = Math.random() * (cmax - cmin) + cmin
        var rsh = Math.random() * (cmax - cmin) + cmin
        MyPalette.primaryColor            = Qt.rgba(rp, rp, rp, MyPalette.primaryColor.a);
        MyPalette.secondaryColor          = Qt.rgba(rs, rs, rs, MyPalette.secondaryColor.a);
        MyPalette.highlightColor          = Qt.rgba(rh, rh, rh, MyPalette.highlightColor.a);
        MyPalette.secondaryHighlightColor = Qt.rgba(rsh, rsh, rsh, MyPalette.secondaryHighlightColor.a);
        MyPalette.highlightBackgroundColor = Theme.highlightBackgroundFromColor( MyPalette.highlightColor, MyPalette.colorScheme );
        MyPalette.highlightDimmerColor = Theme.highlightDimmerFromColor( MyPalette.highlightColor, MyPalette.colorScheme );
        color = Theme.highlightFromColor( MyPalette.primaryColor, Theme.colorScheme);
        rndRemorse.execute( qsTr("Generated") +  " " + text + "…",   function () { }, 2400 )
      }
    }
}

// vim: expandtab ts=4 st=4
