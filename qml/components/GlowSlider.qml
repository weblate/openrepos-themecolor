import QtQuick 2.6
import QtQml.Models 2.1
import Sailfish.Silica 1.0
import "."

Row {
    width: page.isLandscape ? parent.width - Theme.itemSizeLarge * 3 : parent.width - Theme.horizontalPageMargin * 2
    GlowIndicator {
        id: dot
    }
    Slider {
        id: glowSlider
        width: parent.width - dot.width - Theme.paddingSmall * 2
        minimumValue: 0.0
        maximumValue: 0.9999
        stepSize: 1/359
        value: MyPalette.highlightColor.hsvHueF ? MyPalette.highlightColor.hsvHueF : 0.5
        color: Theme.highlightFromColor(Qt.hsva(value, 1.0, 0.5, 0.0), MyPalette.colorScheme)
        property real hue

        onValueChanged: {
          hue = value
          glowSlider.enabled =  false
          MyPalette.backgroundGlowColor = Theme.highlightFromColor(Qt.hsva(hue, 0.5, 0.5, 0.0), MyPalette.colorScheme)
          glowSlider.enabled =  true
        }
    }
}

// vim: expandtab ts=4 st=4
