import QtQuick 2.6
import Sailfish.Silica 1.0
import Nemo.Notifications 1.0

SilicaItem {
  property bool bought: false
  property int numtapped: 0

  anchors.horizontalCenter: parent.horizontalCenter
  width: saver.isLandscape ? parent.width - Theme.itemSizeLarge * 2 : parent.width
  height: col.height
  // hehehe
  RemorsePopup {
    id: buyersRemorse
    TouchBlocker { anchors.fill: parent }
    onTriggered: flick.scrollToBottom()
  }
  Notification {
    id: deliverednotification
    appIcon: "image://theme/icon-m-ambience"
    appName: qsTr("ThemeColor")
    icon: "image://theme/icon-lock-installed"
    category: "transfer.complete"
    itemCount: 2
    summary: qsTr("A Lootbox was delivered!")
    body: qsTr("Your persistence has been rewarded.") + " " + qsTr("ThemeColor") + " " + qsTr("now has more shelves!")
    previewSummary: qsTr("Your persistence has been rewarded!")
  }

  Column {
    id: col
    anchors.horizontalCenter: parent.horizontalCenter
    width: parent.width
    spacing: Theme.paddingLarge
    SectionHeader {
       id: pohdr
       text: qsTr("Purchase Options")
       font.pixelSize: Theme.fontSizeMedium
       color: Theme.primaryColor
    }
    PayPalButton {
      id: ppb
      anchors.horizontalCenter: parent.horizontalCenter
      color: Theme.secondaryHighlightColor
      width: Theme.buttonSizeMedium + Theme.iconSizeMedium
      text: bought ? qsTr("Payment* received!") : qsTr("Buy more shelves")
      enabled: !bought
      blocked: bought
      onClicked: {
        numtapped += 1;
        buyersRemorse.execute(qsTr("Using Jolla Shop credentials to buy Storage Lootbox") + "…", 
            function() {
              bought = true;
              if ( numtapped >= 3 ) { deliverednotification.publish() }
            } , 6000)
      }
    }
    Label {
      id: thanks
      visible: bought
      width: parent.width - Theme.itemSizeMedium * 2
      anchors.horizontalCenter: ppb.horizontalCenter
      color: Theme.primaryColor
      text: qsTr("Thank you for your purchase!<br />Your extra shelves will be delivered in the next update!")
      horizontalAlignment: Text.AlignHCenter
      wrapMode: Text.WordWrap
      font.pixelSize: Theme.fontSizeMedium
    }
    Label {
      visible: bought
      width: parent.width - Theme.itemSizeSmall * 2
      anchors.horizontalCenter: thanks.horizontalCenter
      color: Theme.secondaryColor
      text: qsTr("*) No seriously there are no in-app-purchases in this app or Lootboxes in the Jolla Shop. That would be ridiculous.<br />No funds were transfered.<br /> In fact, nothing really happened just now.<br />…or did it?")
      wrapMode: Text.WordWrap
      font.pixelSize: Theme.fontSizeTiny
    }
  }
}

// vim: expandtab ts=4 st=4
