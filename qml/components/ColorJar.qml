import QtQuick 2.6
import Sailfish.Silica 1.0

IconButton {
  property color col
  property color bgcol: "transparent"
  anchors.topMargin: Theme.paddingSmall;
  icon.sourceSize.height: Theme.iconSizeMedium;
  icon.source: "image://theme/icon-m-delete?" + col
  TouchBlocker { anchors.fill: parent }
  Rectangle {
    color: Qt.colorEqual(parent.bgcol, "transparent") ? "transparent" : Theme.rgba(parent.bgcol, Theme.opacityHigh)
    anchors.fill: parent
    radius: Theme.paddingMedium
  }
}

// vim: expandtab ts=4 st=4
