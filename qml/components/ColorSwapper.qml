import QtQuick 2.6
import Sailfish.Silica 1.0
import "."

SilicaItem {

    property color p
    property color s
    property color h
    property color sh

    property var map: [ p, s, h, sh ]

    property bool initialized: false;
    property bool copy: false;
    Component.onCompleted: { initMap(); initialized = true }

    function initMap() {
      p = MyPalette.primaryColor;
      s = MyPalette.secondaryColor;
      h = MyPalette.highlightColor;
      sh = MyPalette.secondaryHighlightColor;
    }

    onMapChanged: {
      if (!initialized) return;
      console.debug("Map changed.");
      console.debug("Map colrs before:\t " + MyPalette.primaryColor  + MyPalette.secondaryColor + MyPalette.highlightColor + MyPalette.secondaryHighlightColor )
      MyPalette.primaryColor             = Theme.rgba(map[0], MyPalette.primaryColor.a);
      MyPalette.secondaryColor           = Theme.rgba(map[1], MyPalette.secondaryColor.a);
      MyPalette.highlightColor           = Theme.rgba(map[2], MyPalette.highlightColor.a);
      MyPalette.secondaryHighlightColor  = Theme.rgba(map[3], MyPalette.secondaryHighlightColor.a);
      console.debug("Map colrs now:\t " + MyPalette.primaryColor  + MyPalette.secondaryColor + MyPalette.highlightColor + MyPalette.secondaryHighlightColor )
    }

    function copyColor(a,b) {
      // trick so we emit a signal on change of map:
      var tmp = map;
      var cola = map[a];
      var colb = map[b];
      tmp[b] = cola;
      map = tmp;
     }
    function swapColor(a,b) {
      console.debug("Swap colrs in map before:\t " + map[0] + map[1] + map[2] + map[3])
      // trick so we emit a signal on change of map:
      var tmp = map;
      //[ tmp[b], tmp[a] ] = [ map[a], map[b] ];
      var cola = map[a];
      var colb = map[b];
      tmp[b] = cola;
      tmp[a] = colb;
      map = tmp;
      console.debug("Swapped: " + a + "/" + b + " " + map[a] + ", " + map[b]);
      console.debug("Swap colrs in map after:\t " + map[0] + map[1] + map[2] + map[3])
    }

    width: parent.width
    //height: colrow.height + row.height + button.height + head.height
    height: col.height

    Column {
      id: col
      width: parent.width
      spacing: Theme.paddingSmall
    SectionHeader { id: head; text: copy ? qsTr("Copy") : qsTr("Swap") ; color: Theme.primaryColor}
    Row {
      id: colrow
      width: row.width
      ColRect { width: parent.width / 2; color: map[cola.currentIndex] }
      ColRect { width: parent.width / 2; color: map[colb.currentIndex] }
    }
    Row {
      id: row
      anchors.horizontalCenter: parent.horizontalCenter
      width: parent.width
      spacing: 0
      ComboBox {
        id: cola
        width: ( parent.width - button.width ) / 2
        anchors.verticalCenter: parent.verticalCenter
        menu: ContextMenu {
          MenuItem{ text: qsTr("Primary Color")}
          MenuItem{ text: qsTr("Secondary Color")}
          MenuItem{ text: qsTr("Highlight Color")}
          MenuItem{ text: qsTr("Secondary Highlight Color")}
        }
      }
      IconButton {
        id: button
        anchors.verticalCenter: parent.verticalCenter
        height: Theme.iconSizeMedium
        width: Theme.iconSizeMedium
        icon.source: copy ? "image://theme/icon-m-data-upload" : "image://theme/icon-m-data-traffic"
        icon.rotation: 90
        enabled: cola.currentIndex !== colb.currentIndex
        onClicked: copy ? copyColor(cola.currentIndex,colb.currentIndex) : swapColor(cola.currentIndex,colb.currentIndex);
      }
      ComboBox {
        id: colb
        //width: ( parent.width - button.width ) / 2
        anchors.verticalCenter: parent.verticalCenter
        menu: ContextMenu {
          MenuItem{ text: qsTr("Primary Color")}
          MenuItem{ text: qsTr("Secondary Color")}
          MenuItem{ text: qsTr("Highlight Color")}
          MenuItem{ text: qsTr("Secondary Highlight Color")}
        }
      }
    }
    }


}

// vim: expandtab ts=4 st=4
