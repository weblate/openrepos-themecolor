import QtQuick 2.6
import Sailfish.Silica 1.0

LinkedLabel {
  anchors.horizontalCenter: parent.horizontalCenter
  width: parent.width - Theme.horizontalPageMargin
  color: Theme.secondaryColor
  font.pixelSize: Theme.fontSizeSmall
  horizontalAlignment: Text.AlignJustify
  wrapMode: Text.WordWrap
  plainText: this.text
}

// vim: expandtab ts=4 st=4
