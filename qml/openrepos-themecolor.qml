/*

MIT License

Copyright (c) 2021 Peter Gantner (nephros)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*/

import QtQuick 2.6
import Sailfish.Silica 1.0
import Nemo.Configuration 1.0
import Nemo.DBus 2.0
import "pages"
import "components"

ApplicationWindow {
    id: app

    // generic interface to SystemD, so we can restart units
    DBusInterface {
        id: systemdbus
        bus: DBus.SessionBus
        service: "org.freedesktop.systemd1"
        path: "/org/freedesktop/systemd1"
        iface: "org.freedesktop.systemd1.Manager"
        function restartAmbienced() {
          call('RestartUnit', [ 'ambienced.service', 'replace' ],
                      function (result) {         console.debug('DBus call result: ' + result) },
                      function (error, message) { console.warn('DBus call failed: ', error, 'message: ', message) }
               )
        }function restartLipstick() {
          call('RestartUnit', [ 'lipstick.service', 'replace' ],
                      function (result) {         console.debug('DBus call result: ' + result) },
                      function (error, message) { console.warn('DBus call failed: ', error, 'message: ', message) }
               )
        }
    }
    // DBus connection, so we can open system settings
    DBusInterface {
        id: settings
        bus: DBus.SessionBus
        service: "com.jolla.settings"
        path: "/com/jolla/settings/ui"
        iface: "com.jolla.settings.ui"
        //signalsEnabled: true
        function open() {
            typedCall("showPage", { "type": "s", "value": "system_settings/look_and_feel/ambiences" },
                        function (result) { console.debug('DBus call result: ' + result)} ,
                        function (error, message) { console.warn('DBus call failed: ', error, 'message: ', message) }
                     )
        }
    }

    // DBus connection to ambienced, to save settings to ambience
    DBusInterface {
        id: ambiencedbus
        bus: DBus.SessionBus
        service: "com.jolla.ambienced"
        path: "/com/jolla/ambienced"
        iface: "com.jolla.ambienced"
        signalsEnabled: true
        // from  http://www.jollausers.com/2013/12/how-to-make-ambiance-wallpapers-for-sailfish-bonus/
        // method void com.jolla.ambienced.createAmbience(QString url)
        //   where url is a real one or a file:/// to an image
        // method void com.jolla.ambienced.saveAttributes(int contentType, qlonglong contentId, QVariantMap args)
        function saveAmbience() {
          call('saveAttributes', [ ],
                      function (result) {         console.debug('DBus call result: ' + result) },
                      function (error, message) { console.warn('DBus call failed: ', error, 'message: ', message) }
              )
        }
        function makeAmbience() {
          call('createAmbience', [ ],
                      function (result) {         console.debug('DBus call result: ' + result) },
                      function (error, message) { console.warn('DBus call failed: ', error, 'message: ', message) }
               );
        }
        // signal handler
        // to detect ambience change
        function contentChanged() { 
          console.debug("ambienced signalled");
          colorsInitialized = false; initColors();
        }
    }

    ConfigurationValue {
        id: devicemodel
        key: "/desktop/lipstick-jolla-home/model"
    }
    ConfigurationValue {
        id: imagepath
        key: "/desktop/jolla/background/portrait/home_picture_filename"
        // now handled by signal above
        //onValueChanged: { colorsInitialized = false; initColors() }
    }
    ConfigurationGroup {
      id: conf
      path: "/desktop/jolla/theme/color"
      synchronous: false
      property color primary
      property color secondary
      property color highlight
      property color secondaryHighlight
      property color highlightBackground
      property color highlightDimmer
      property color backgroundGlow
    }

    property bool colorsInitialized: false

    allowedOrientations: (devicemodel === 'planetgemini') ?  Orientation.LandscapeInverted : defaultAllowedOrientations

    function initColors() {
      if ( !colorsInitialized ) {
         // dump all Theme colors:
         console.info("Theme colors:\n" +
             "primaryColor: " + Theme.primaryColor + "\n" +
             "secondaryColor: " + Theme.secondaryColor + "\n" +
             "highlightColor: " + Theme.highlightColor + "\n" +
             "secondaryHighlightColor: " + Theme.secondaryHighlightColor + "\n" +
             "highlightBackgroundColor: " + Theme.highlightBackgroundColor + "\n" +
             "highlightDimmerColor: " + Theme.highlightDimmerColor + "\n" +
             "overlayBackgroundColor: " + Theme.overlayBackgroundColor + "\n" +
             "backgroundGlowColor: " + Theme.backgroundGlowColor + "\n" +
             "_wallpaperOverlayColor: " + Theme._wallpaperOverlayColor + "\n" +
             "_coverOverlayColor: " + Theme._coverOverlayColor + "\n"
             );


         MyPalette.primaryColor = conf.primary
         MyPalette.secondaryColor = conf.secondary
         MyPalette.highlightColor = conf.highlight
         MyPalette.secondaryHighlightColor = conf.secondaryHighlight
         // these will not exist at first:
         if ( typeof(MyPalette.colorScheme) === "undefined" ) { MyPalette.colorScheme = Theme.colorScheme }
         if ( typeof(conf.backgroundGlow) === "undefined" ) { MyPalette.backgroundGlowColor = Theme.backgroundGlowColor } else { MyPalette.backgroundGlowColor = conf.backgroundGlow }
         if ( typeof (conf.highlightBackground) === "undefined" ) {
           //MyPalette.highlightBackgroundColor = Theme.highlightBackgroundFromColor( MyPalette.highlightColor, MyPalette.colorScheme );
           MyPalette.highlightBackgroundColor = Theme.highlightBackgroundColor;
         } else { MyPalette.highlightBackgroundColor = conf.highlightBackground }
         if ( typeof(conf.highlightDimmer ) === "undefined") {
           //MyPalette.highlightDimmerColor = Theme.highlightDimmerFromColor( MyPalette.highlightColor, MyPalette.colorScheme );
           MyPalette.highlightDimmerColor = Theme.highlightDimmerColor;
         } else { MyPalette.highlightDimmerColor = conf.highlightDimmer }

         colorsInitialized = true;
      }
      console.debug("init colors")
    }
    function computeColors() {
      MyPalette.primaryColor =   (Theme.colorScheme == Theme.LightOnDark) ? "#FFFFFFFF" : "#FF000000";
      MyPalette.secondaryColor = (Theme.colorScheme == Theme.LightOnDark) ? "#B0FFFFFF" : "#B0000000";
      // TODO: what to choose here exactly:
      MyPalette.highlightColor              = Theme.highlightFromColor(MyPalette.highlightColor, MyPalette.colorScheme)
      MyPalette.secondaryHighlightColor     = Theme.secondaryHighlightFromColor(MyPalette.highlightColor, MyPalette.colorScheme)
      MyPalette.highlightBackgroundColor    = Theme.highlightBackgroundFromColor(MyPalette.highlightColor, MyPalette.colorScheme)
      MyPalette.highlightDimmerColor        = Theme.highlightDimmerFromColor(MyPalette.highlightColor, MyPalette.colorScheme)
      MyPalette.backgroundGlowColor         = Theme.backgroundGlowColor
    }

    function reloadThemeColors() {
      MyPalette.primaryColor                = Theme.primaryColor;
      MyPalette.secondaryColor              = Theme.primaryColor;
      MyPalette.highlightColor              = Theme.highlightColor;
      MyPalette.secondaryHighlightColor     = Theme.secondaryHighlightColor;
      MyPalette.highlightBackgroundColor    = Theme.highlightBackgroundColor;
      MyPalette.highlightDimmerColor        = Theme.highlightDimmerColor;
      MyPalette.backgroundGlowColor         = Theme.backgroundGlowColor
    }

    function applyThemeColors() {
      conf.primary = MyPalette.primaryColor
      conf.secondary = MyPalette.secondaryColor
      conf.highlight = MyPalette.highlightColor
      conf.secondaryHighlight = MyPalette.secondaryHighlightColor
      conf.highlightBackground = MyPalette.highlightBackgroundColor
      conf.highlightDimmer = MyPalette.highlightDimmerColor
      conf.backgroundGlow = MyPalette.backgroundGlowColor
      conf.sync()
    }
    function restartAmbienced() {
      systemdbus.restartAmbienced()
    }
    function restartLipstick() {
      systemdbus.restartLipstick()
    }
    function resetDconf() {
      conf.clear();
      conf.primary =   (Theme.colorScheme == Theme.LightOnDark) ? "#FFFFFFFF" : "#FF000000";
      conf.secondary = (Theme.colorScheme == Theme.LightOnDark) ? "#B0FFFFFF" : "#B0000000";
      conf.highlight = Theme.highlightColor;
      conf.secondaryHighlight = Theme.secondaryHighlightColor;
      conf.sync();
    }

    initialPage: Component { FirstPage{} }
    cover: Qt.resolvedUrl("cover/CoverPage.qml")
    //PageBusyIndicator { running: app.status === Component.Loading }

}

// vim: expandtab ts=4 st=4
