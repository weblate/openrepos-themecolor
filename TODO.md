## TODOs:

 * [] autocompute theme
 * [] export ambience file/ generate ambience RPM
 * [] listen on dbus config changed signal and apply stored ambience config automatically
 * [] support backgroundGlowColor
 * [x] add reset to default option (https://openrepos.net/comment/37079#comment-37079)
 * [x] handle ambience changed dbus signal
 * [x] save slots
